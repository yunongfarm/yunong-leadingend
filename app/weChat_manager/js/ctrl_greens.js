function ctrl_greens($scope, $http, Page, $state, $stateParams) {
    //config
    Page.setTitle('种子');

    //获取商品信息
    var xhr = new Jsonp($http);

    get_seed_msg();

    function get_seed_msg() {
        xhr.get(_SERVER_ + "/cropAllGet?access_token=" + _ACCESSTOKEN_ + "&callback=JSON_CALLBACK", function(data) {
            log(data.crops);
            $scope.greens = data.crops;
            $scope.farm = data.farmConstantModel;
        });
    }

    /**
     * hint init
     * @type {Object}
     */
    $scope.hintdate = {
        content: '最多只能选3种作物喔~',
        show: false
    };

    $scope.green_add = function(id) {
        $state.go('greens.add', {
            id: id
        });
    }
    $scope.green_edit = function(id) {
        $state.go('greens.edit', {
            id: id
        });
    }

    function hint_delay(content) {
        $scope.hintdate = {
            content: content,
            show: false
        };
        hint.delay(4000);
    }

    // 弹出层管理器
    var dialog = new Popup($scope, 'toastdata');
    var hint = new Popup($scope, 'hintdate');

}

function ctrl_greens_edit($scope, $http, Page, $state, $stateParams) {
    var id = $stateParams.id;

    $scope.update = function() {
        log($scope.green)
        // log($scope.green_status)
        var green = $scope.green;
        var cropId = green.id,
            grownDay = green.grownDay,
            grownUpDay = green.grownUpDay,
            status = $scope.green_status.status,
            price = green.price,
            image = $$('img_box').firstElementChild.src,
            name = green.name;

        var update_crop_url = _SERVER_ + "/cropDataUpdate?access_token=" + _ACCESSTOKEN_ + "&cropId=" + cropId + "&grownDay=" + grownDay + "&grownUpDay=" + grownUpDay + "&status=" + status + "&price=" + price + "&image=" + image + "&name=" + name + "&callback=JSON_CALLBACK";
        xhr.get(update_crop_url, function(data) {
            log(data);
            alert("更新成功");
            get_seed_msg(id);
        });
    };

    $scope.status = [
        { status: 0, name: "否" },
        { status: 1, name: "是" }
    ];

    $scope.isJudge = function(bool) {
        if (bool) return "是";
        else return "否";
    }

    // /获取商品信息
    var xhr = new Jsonp($http);

    get_seed_msg(id);

    function get_seed_msg(id) {
        xhr.get(_SERVER_ + "/cropAllGet?access_token=" + _ACCESSTOKEN_ + "&callback=JSON_CALLBACK", function(data) {
            log(data.crops[id]);
            $scope.green = data.crops[id];
            // $scope.farm = data.farmConstantModel;
        });
    }

    function reader_img(id, img_url) {
        if (img_url) {
            var container = document.getElementById(id);
            var img = document.createElement('img');
            img.src = img_url;
            if (!container.hasChildNodes()) {
                container.appendChild(img);
            } else {
                container.replaceChild(img, container.firstElementChild);
            }
        }
    };

    //init上传环境
    var f_name = '';
    var file_type = [];
    var uploader = Qiniu.uploader({
        runtimes: 'html5,flash,html4',
        browse_button: 'pickfiles',
        container: 'container',
        drop_element: 'container',
        flash_swf_url: 'bower_components/plupload/js/Moxie.swf',
        dragdrop: false,
        log_level: 3,
        //chunk_size: '4mb',
        multi_selection: false,
        uptoken_func: function() {
            console.log(file_type);
            var type = '.' + file_type.shift();
            //请求构造
            var q_url = 'http://106.75.137.2:8888/api/admin/pedia/up_token/?filename=';
            f_name = generateMixed(64) + type;
            q_url = q_url + f_name;
            var uptoken = '';
            var xhr = createXHR();

            xhrthen(xhr, function() {
                respon = JSON.parse(xhr.responseText);
                console.log(respon)
                uptoken = respon.up_token;
            });

            xhr.open('get', q_url, false);
            xhr.send();
            console.log(f_name)
            console.log(uptoken)
            return uptoken;
        },

        domain: "http://o8covea2w.bkt.clouddn.com/",
        get_new_uptoken: true,
        resize: {
            width: 512,
            height: 512,
            crop: true,
            quality: 100,
            preserve_headers: false
        },
        auto_start: true,
        init: {
            'QueueChanged': function(up) {
                console.log('change')
            },
            'FileFiltered': function(up, file) {
                console.log('file_init')
                var type = file.type.replace(/image\//g, '');
                console.log(type)
                file_type.push(type);
            },
            'FilesAdded': function(up, files) {
                console.log('add')
                console.log(files)
            },
            'BeforeUpload': function(up, file) {
                console.log('before')
            },
            'UploadFile': function(up, file) {
                console.log('upload')
            },
            'UploadProgress': function(up, file) {},
            'UploadComplete': function() {
                file_type = [];
            },
            'FileUploaded': function(up, file, info) {
                console.log(info)
                var domain = up.getOption('domain');
                var res = eval('(' + info + ')');
                var sourceLink = domain + res.key; //获取上传文件的链接地址
                console.log(sourceLink);
                reader_img('img_box', sourceLink);
            },
            'Error': function(up, err, errTip) {

            },
            'Key': function(up, file) {
                var key = f_name;
                return key
            }
        }
    });
}

function ctrl_greens_add($scope, $http, Page, $state, $stateParams) {
    // var id = $stateParams.id;
    // /获取商品信息
    var xhr = new Jsonp($http);
    $scope.green = {
        grownDay: "",
        grownUpDay: "",
        status: "",
        price: "",
        image: "",
        name: ""
    }

    $scope.add = function() {
        log($scope.green)
        log($$('img_box').firstElementChild.src)
        var green = $scope.green;
        var grownDay = green.grownDay,
            grownUpDay = green.grownUpDay,
            status = green.status,
            price = green.price,
            image = $$('img_box').firstElementChild.src,
            name = green.name;

        var update_crop_url = _SERVER_ + "/cropDataAdd?access_token=" + _ACCESSTOKEN_ + "&grownDay=" + grownDay + "&grownUpDay=" + grownUpDay + "&status=" + status + "&price=" + price + "&image=" + image + "&name=" + name + "&callback=JSON_CALLBACK";
        xhr.get(update_crop_url, function(data) {
            log(data);
        });
    };



    function reader_img(id, img_url) {
        if (img_url) {
            var container = document.getElementById(id);
            var img = document.createElement('img');
            img.src = img_url;
            if (!container.hasChildNodes()) {
                container.appendChild(img);
            } else {
                container.replaceChild(img, container.firstElementChild);
            }
        }
    };

    //init上传环境
    var f_name = '';
    var file_type = [];
    var uploader = Qiniu.uploader({
        runtimes: 'html5,flash,html4',
        browse_button: 'pickfiles',
        container: 'container',
        drop_element: 'container',
        flash_swf_url: 'bower_components/plupload/js/Moxie.swf',
        dragdrop: false,
        log_level: 3,
        //chunk_size: '4mb',
        multi_selection: false,
        uptoken_func: function() {
            console.log(file_type);
            var type = '.' + file_type.shift();
            //请求构造
            var q_url = 'http://106.75.137.2:8888/api/admin/pedia/up_token/?filename=';
            f_name = generateMixed(64) + type;
            q_url = q_url + f_name;
            var uptoken = '';
            var xhr = createXHR();

            xhrthen(xhr, function() {
                respon = JSON.parse(xhr.responseText);
                console.log(respon)
                uptoken = respon.up_token;
            });

            xhr.open('get', q_url, false);
            xhr.send();
            console.log(f_name)
            console.log(uptoken)
            return uptoken;
        },

        domain: "http://o8covea2w.bkt.clouddn.com/",
        get_new_uptoken: true,
        resize: {
            width: 512,
            height: 512,
            crop: true,
            quality: 100,
            preserve_headers: false
        },
        auto_start: true,
        init: {
            'QueueChanged': function(up) {
                console.log('change')
            },
            'FileFiltered': function(up, file) {
                console.log('file_init')
                var type = file.type.replace(/image\//g, '');
                console.log(type)
                file_type.push(type);
            },
            'FilesAdded': function(up, files) {
                console.log('add')
                console.log(files)
            },
            'BeforeUpload': function(up, file) {
                console.log('before')
            },
            'UploadFile': function(up, file) {
                console.log('upload')
            },
            'UploadProgress': function(up, file) {},
            'UploadComplete': function() {
                file_type = [];
            },
            'FileUploaded': function(up, file, info) {
                console.log(info)
                var domain = up.getOption('domain');
                var res = eval('(' + info + ')');
                var sourceLink = domain + res.key; //获取上传文件的链接地址
                console.log(sourceLink);
                reader_img('img_box', sourceLink);
            },
            'Error': function(up, err, errTip) {

            },
            'Key': function(up, file) {
                var key = f_name;
                return key
            }
        }
    });
}
