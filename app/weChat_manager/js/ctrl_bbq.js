var data = {
    bbqid: '1'
}

function ctrl_bbq($scope) {

}

function ctrl_bbq_edit($scope) {
    //图片处理
    function get_imgid(id) {
        return 'image' + id;
    }

    function reader_img(id, img_url) {
        if (img_url) {
            var container = document.getElementById(id);
            var img = document.createElement('img');
            img.src = img_url;
            if (!container.hasChildNodes()) {
                container.appendChild(img);
            } else {
                container.replaceChild(img, container.firstElementChild);
            }
        }
    };
    $scope.fileNameChanged = function(elem) {
        console.log(elem.dataset.class)

        var img_box = get_imgid(elem.id);

        var ifile = $$(elem.id);
        console.log(ifile)
        uploader.setOption('img_box', img_box)
        uploader.addFile(ifile)
        console.log(uploader)
    };

    //init上传环境
    var f_name = '';
    var file_type = [];
    var uploader = Qiniu.uploader({
        runtimes: 'html5,flash,html4',
        browse_button: 'pickfiles',
        container: 'container',
        drop_element: 'container',
        flash_swf_url: 'bower_components/plupload/js/Moxie.swf',
        dragdrop: false,
        log_level: 3,
        //chunk_size: '4mb',
        multi_selection: false,
        uptoken_func: function() {
            console.log(file_type);
            var type = '.' + file_type.shift();
            //请求构造
            var q_url = 'http://106.75.137.2:8888/api/admin/pedia/up_token/?filename=';
            f_name = generateMixed(64) + type;
            q_url = q_url + f_name;
            var uptoken = '';
            var xhr = createXHR();

            xhrthen(xhr, function() {
                respon = JSON.parse(xhr.responseText);
                console.log(respon)
                uptoken = respon.up_token;
            });

            xhr.open('get', q_url, false);
            xhr.send();
            console.log(f_name)
            console.log(uptoken)
            return uptoken;
        },

        domain: "http://o8covea2w.bkt.clouddn.com/",
        get_new_uptoken: true,
        resize: {
            width: 512,
            height: 384,
            crop: true,
            quality: 100,
            preserve_headers: false
        },
        auto_start: true,
        init: {
            'QueueChanged': function(up) {
                console.log('change')
            },
            'FileFiltered': function(up, file) {
                console.log('file_init')
                var type = file.type.replace(/image\//g, '');
                console.log(type)
                file_type.push(type);
            },
            'FilesAdded': function(up, files) {
                console.log('add')
                console.log(files)
                loading.show();
            },
            'BeforeUpload': function(up, file) {
                console.log('before')
            },
            'UploadFile': function(up, file) {
                console.log('upload')
            },
            'UploadProgress': function(up, file) {},
            'UploadComplete': function() {
                file_type = [];
            },
            'FileUploaded': function(up, file, info) {
                console.log(info)
                var domain = up.getOption('domain');
                var res = eval('(' + info + ')');
                var sourceLink = domain + res.key; //获取上传文件的链接地址
                console.log(sourceLink);
                console.log(up.getOption('img_box'))
                reader_img(up.getOption('img_box'), sourceLink);
                loading.hide();
            },
            'Error': function(up, err, errTip) {

            },
            'Key': function(up, file) {
                var key = f_name;
                return key
            }
        }
    });
}
