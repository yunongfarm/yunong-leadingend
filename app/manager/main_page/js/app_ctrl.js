/**
 * app管理者控制器
 * @authors Your Name (you@example.org)
 * @date    2016-07-08 09:59:43
 * @version $Id$
 */
/**
 * -------------------------------------------------------------------------------------------app管理者模块
 * @param  {[type]} $stateParams [description]
 * @return {[type]}              [description]
 */
function Article($http, $stateParams, $state, loading, md) {
    this.publish = function() {
        loading.show();
        if (document.forms[0].checkValidity()) {
            //在http中嵌套$stateParams会出现bad request
            var pedia_id = $stateParams.pedia_id;
            var context = '<link href="http://106.75.137.2:8888/static/css/default.css" rel="stylesheet">' + $$('preview').innerHTML;
            //init_md
            md.set_form({
                pedia_id: pedia_id,
                title: $$('md_category').value,
                city: $stateParams.city,
                province: $stateParams.province,
                category: $$('md_category').value,
                context: context,
                location: '(' + $stateParams.location + ')',
                markdown: $$('text-input').value
            });
            try {
                md.set('pushpin', $$('icon_box').firstElementChild.src);
            } catch (err) {
                console.log(err)
            }
            console.log(md.get_form())

            $http({
                url: __API__._AddPediaF_,
                method: pedia_id ? 'PUT' : 'POST', //put更新,post添加
                data: md.get_form(),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function(data, status, headers, config) {
                console.log(data);
                /**
                 * 进入更新/新增逻辑状态->refash
                 * @type {[type]}
                 */
                $state.go('app_mager__gar_recom', {
                    provinces: $stateParams.province,
                    city: $stateParams.city,
                    location: $stateParams.location,
                    pedia_id: data.pedia_id | $stateParams.pedia_id
                });
                alert('发布 / 更新 成功！')
                loading.hide();
            }).error(function(data, header, config, status) {
                console.log(data);
                console.log(header);
                console.log(status);
                alert('请检查网络连接')
            });
        }
    };
    this.get = function(pedia_id, this_url) {
        loading.show();
        if (pedia_id) {
            $http({
                url: this_url + pedia_id,
                method: "GET",
                data: '',
                // params: {},
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function(data, status, headers, config) {
                //update_msg
                log(data)
                md.set_form(data);
                console.log(md.get_form());
                console.log(data.location);

                //渲染必要信息
                md.render_input('md_category', md.get('category'));
                md.render_input('text-input', md.get('markdown'));
                md.render_elem('preview', md.get('context'));
                md.render_img('icon_box', md.get('pushpin'));
                md.render_img('cover_box', md.get('coverurl'));

                loading.hide();
            }).error(function(data, header, config, status) {
                console.log(data);
                console.log(header);
                console.log(status);
                alert('请检查网络连接')
            });
        } else return (function() {
            loading.hide();
        })();
    };
}

function Intro($http) {

    this.publish = function(this_url, f_obj) {
        var f_obj = f_obj;
        if (document.forms['main_box'].checkValidity()) {
            var context = '<link href="http://106.75.137.2:8888/static/css/default.css" rel="stylesheet">' + $$('preview').innerHTML;
            // init
            f_obj.init();
            $http({
                url: this_url,
                method: f_obj.data.intro_id ? 'PUT' : 'POST', //put更新,post添加
                data: f_obj.data,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function(data, status, headers, config) {
                f_obj.callback();
                alert('发布 / 更新 成功！')
            }).error(function(data, header, config, status) {
                console.log(data);
                console.log(header);
                console.log(status);
                alert('请检查网络连接')
            });
        }
    };
    this.get = function(pedia_id, this_url, obj) {
        obj.init();
        if (pedia_id) {
            $http({
                url: this_url + pedia_id,
                method: "GET",
                data: '',
                // params: {},
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function(data, status, headers, config) {
                //update_msg
                log(data)
                obj.callback(data);
            }).error(function(data, header, config, status) {
                console.log(data);
                console.log(header);
                console.log(status);
                alert('请检查网络连接')
            });
        } else return (function() {
            loading.hide();
        })();
    };
}

function ctrl_app_mager__gar_recom($scope, $stateParams, $http, $state) {
    //page_init
    (function() {
        $scope.upload_size = 0;
        $scope.upload_ed = 0;
        $scope.upload_pc = 0;
        $scope.upload_byte = 0;
        $scope.back = function() {
            $state.go('app_mager__plant_science');
        }
    })();
    console.log($stateParams);
    var pedia_id = $stateParams.pedia_id;
    var md = new EditForm();
    md.init({
        pedia_id: '',
        title: '',
        city: '',
        province: '',
        pushpin: '',
        category: '',
        context: '',
        location: '',
        markdown: '',
        coverurl: ''
    });
    var mark_node = $$('md_waiting');
    var progerss_node = $$('fsUploadProgress');
    var loading = new Loading(mark_node);
    var progress = new Loading(progerss_node);
    var art = new Article($http, $stateParams, $state, loading, md);
    //拉取文章
    art.get(pedia_id, __API__._GetAtc_);

    $$('full_screen_toggle').onclick = function(ev) {
        full_screen_toggle('main_box');
    }

    /**
     * 文章发布&更新
     * @type {[type]}
     */
    var form = $$('main_box');
    form.onsubmit = function(e) {
        e.preventDefault();
        art.publish();
    };
    /**
     * markdowm 插件
     * @param {[type]} input   [description]
     * @param {[type]} preview [description]
     */
    function Editor(input, preview) {
        this.update = function() {
            preview.innerHTML = markdown.toHTML(input.value);
        };
        input.editor = this;
        this.update();
    }

    new Editor($$("text-input"), $$("preview"));

    /**
     * 七牛模块
     * @return {[type]} [description]
     */
    (function() {
        $$('icon_pickfiles').onclick = function(ev) {
            uploader.setOption('toggle_btn', 'icon');
        };

        $$('cover_pickfiles').onclick = function(ev) {
            uploader.setOption('toggle_btn', 'cover');
        };

        var tBox = document.querySelector('.drop-box');
        //目标box绑定dnd事件
        tarBoxBind(tBox);
        //取消document dnd事件
        retarBoxBind(document);

        function tarBoxBind(tBox) {
            tBox.ondragover = function(ev) {
                console.log('over');
                ev.preventDefault();
            }
            tBox.ondragenter = function(ev) {
                console.log('enter');
            }
            tBox.ondrop = function(ev) {
                console.log('ondrop')
                uploader.setOption('toggle_btn', 'drop');
            }
            tBox.ondragleave = function(ev) {
                console.log('dleave');
            }
        }

        function retarBoxBind(tBox) {
            tBox.ondragover = function(ev) {
                ev.preventDefault();
            }
            tBox.ondragenter = function(ev) {
                ev.preventDefault();
            }

            tBox.ondrop = function(ev) {
                ev.preventDefault();
            }
            tBox.ondragleave = function(ev) {
                ev.preventDefault();
            }
        }

        var f_name = '';
        var file_type = [];

        var uploader = Qiniu.uploader({
            runtimes: 'html5,flash,html4',
            browse_button: ['icon_pickfiles', 'cover_pickfiles'], //上传按钮的ID
            container: 'btn-uploader', //上传按钮的上级元素ID
            drop_element: 'text-input',
            max_file_size: '8m', //最大文件限制
            flash_swf_url: '/static/js/plupload/Moxie.swf',
            dragdrop: true,
            // chunk_size: '512kb', //分块大小
            uptoken_func: function() {
                console.log(file_type);
                var type = '.' + file_type.shift();
                //请求构造
                var q_url = 'http://106.75.137.2:8888/api/admin/pedia/up_token/?filename=';
                f_name = generateMixed(64) + type;
                q_url = q_url + f_name;
                var uptoken = '';
                var xhr = createXHR();
                /**
                 * callback
                 * @param  {[type]} ) {                       respon [description]
                 * @return {[type]}   [description]
                 */
                xhrthen(xhr, function() {
                    respon = JSON.parse(xhr.responseText);
                    console.log(respon)
                    uptoken = respon.up_token;
                });

                xhr.open('get', q_url, false);
                xhr.send();
                console.log(f_name)
                console.log(uptoken)
                return uptoken;
            },
            //设置请求qiniu-token的url
            //Ajax请求upToken的Url，**强烈建议设置**（服务端提供）
            // uptoken : '<Your upload token>',
            //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
            // unique_names: true,
            // 默认 false，key为文件名。若开启该选项，SDK会为每个文件自动生成key（文件名）
            // save_key: true,
            // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK在前端将不对key进行任何处理
            domain: 'http://o8covea2w.bkt.clouddn.com/', //自己的七牛云存储空间域名
            //multi_selection: true, //是否允许同时选择多文件
            //文件类型过滤，这里限制为图片类型
            filters: {
                mime_types: [{
                    title: "Image files",
                    extensions: "jpg,jpeg,gif,png"
                }]
            },
            get_new_uptoken: true,
            //每次get新的token
            auto_start: true,
            resize: {
                width: 512,
                height: 384,
                crop: true,
                quality: 100,
                preserve_headers: false
            },
            init: {
                //init_file该事件会在每一个文件被添加到上传队列前触发
                'FileFiltered': function(up, file) {
                    loading.show();
                    progress.show();
                    console.log('file_init');
                    console.log(file.size / 1024);

                    var type = file.type.replace(/image\//g, '');
                    console.log(type)
                    file_type.push(type);
                    // var fsize = Math.floor(file.size / 1024);
                    // up.stop();
                    // console.log('waiting');
                    // if (fsize >= 0) {
                    //     //基准宽
                    //     var base_w = 512;

                    //     var render = new Filerender();
                    //     render.readAsDataURL(file.getNative());
                    //     render.onload = (function(e) {
                    //         var image = new Image();
                    //         image.src = e.target.result;
                    //         image.onload = function() {
                    //             var scale = this.width / this.height;
                    //             var autoh = get_autoh(base_w, scale);

                    //             resize_img(base_w, autoh);

                    //             up.addFile(image)
                    //         };
                    //     });
                    //     render.onloadend = function(e) {
                    //         console.log('loadend')

                    //     };

                    // } else {
                    //     console.log('can_not_Resize')
                    // }

                    // function resize_img(width, height) {
                    //     var resize = {
                    //         width: Math.floor(width),
                    //         height: Math.floor(height),
                    //         crop: false,
                    //         quality: 90,
                    //         preserve_headers: true
                    //     }
                    //     up.setOption('resize', resize);
                    // }

                    // function get_autoh(width, scale) {
                    //     return width / scale;
                    // }
                },
                'FilesAdded': function(up, files) {
                    console.log('f_add')
                    console.log(files)
                },
                'FilesRemoved': function(up, files) {
                    console.log('remove')
                    console.log(files)
                },
                'BeforeUpload': function(up, file) {
                    console.log('upload_Before');
                },
                'UploadFile': function(up, file) {
                    console.log('upFile');
                    console.log(up.getOption('resize'));
                },
                'UploadProgress': function(up, file) {
                    console.log(file.size / 1024);
                    display_progress();

                    //可以在这里控制上传进度的显示
                    //可参考七牛的例子
                    function display_progress() {
                        console.log(uploader.total)
                        console.log($scope.upload_pc)
                        $scope.upload_size = Math.floor(uploader.total.size / 1024);
                        $scope.upload_ed = Math.floor(uploader.total.loaded / 1024);
                        $scope.upload_pc = uploader.total.percent;
                        $scope.upload_byte = Math.floor(uploader.total.bytesPerSec / 1024);
                        $scope.$digest()
                    }

                },
                'UploadComplete': function() {
                    loading.hide();
                    progress.hide();
                    file_type = [];
                },
                'FileUploaded': function(up, file, info) {
                    //每个文件上传成功后,处理相关的事情
                    //其中 info 是文件上传成功后，服务端返回的json，形式如
                    //{
                    //  "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
                    //  "key": "gogopher.jpg"
                    //}
                    /**
                     * 图片上传策略
                     */
                    console.log(info)
                    var domain = up.getOption('domain');
                    var res = eval('(' + info + ')');
                    var sourceLink = domain + res.key; //获取上传文件的链接地址
                    console.log(sourceLink);
                    /**
                     * 分发拖拽上传&点击上传
                     * @param  {[type]} uploader.getOption('isdrop') [description]
                     * @return {[type]}                              [description]
                     */
                    var toggle_btn = uploader.getOption('toggle_btn');
                    //三种渲染状态分发
                    switch (toggle_btn) {
                        case 'icon':
                            console.log('icon_upload')
                            md.set('pushpin', sourceLink);
                            md.render_img('icon_box', sourceLink);
                            break;
                        case 'cover':
                            console.log('cover_upload')
                            md.set('coverurl', sourceLink);
                            md.render_img('cover_box', sourceLink);
                            break;
                        case 'drop':
                            console.log('drop_upload')
                            var textarea = document.getElementById('text-input');
                            var img_tem = "![Alt text]( " + sourceLink + " '在编辑提示文字')";
                            var value = textarea.value;
                            value += img_tem;
                            textarea.value = value;
                            break;
                    }

                },
                'Error': function(up, err, errTip) {
                    console.log(err)
                    alert(errTip);
                },
                'Key': function(up, file) {
                    var key = f_name;
                    return key
                }
            }
        });

    })();
}

function ctrl_app_mager__intro($scope, $stateParams, $http, $state) {
    //page_init
    (function() {
        $scope.upload_size = 0;
        $scope.upload_ed = 0;
        $scope.upload_pc = 0;
        $scope.upload_byte = 0;
        $scope.back = function() {
            $state.go('main_select');
        }
    })();
    console.log($stateParams);
    var pedia_id = '1';
    var md = new EditForm();
    md.init({
        intro_id: '',
        title: '',
        context: '',
        markdown: '',
        coverurl: ''
    });
    var mark_node = $$('md_waiting');
    var progerss_node = $$('fsUploadProgress');
    var loading = new Loading(mark_node);
    var progress = new Loading(progerss_node);
    var art = new Intro($http);

    //拉取文章
    art.get(pedia_id, __API__._GetIntro_, {
        init: function() {
            loading.show();
        },
        callback: function(data) {
            md.set_form(data);
            console.log(md.get_form());

            //渲染必要信息
            md.render_input('text-input', md.get('markdown'));
            md.render_input('md_title', md.get('title'));
            md.render_elem('preview', md.get('context'));
            md.render_img('cover_box', md.get('coverurl'));

            loading.hide();
        }
    });

    $$('full_screen_toggle').onclick = function(ev) {
        full_screen_toggle('main_box');
    }

    /**
     * 文章发布&更新
     * @type {[type]}
     */
    var form = $$('main_box');
    form.onsubmit = function(e) {
        e.preventDefault();
        art.publish(__API__._UpIntro_, {
            data: md.get_form(),
            init: function() {
                loading.show();
                md.set_form({
                    pedia_id: pedia_id,
                    title: $$('md_title').value,
                    context: $$('preview').innerHTML,
                    markdown: $$('text-input').value
                });
                try {
                    md.set('pushpin', $$('icon_box').firstElementChild.src);
                } catch (err) {
                    console.log(err)
                }
                console.log(md.get_form())
            },
            callback: function() {
                loading.hide();
            }
        });
    };
    /**
     * markdowm 插件
     * @param {[type]} input   [description]
     * @param {[type]} preview [description]
     */
    function Editor(input, preview) {
        this.update = function() {
            preview.innerHTML = markdown.toHTML(input.value);
        };
        input.editor = this;
        this.update();
    }

    new Editor($$("text-input"), $$("preview"));

    /**
     * 七牛模块
     * @return {[type]} [description]
     */
    (function() {
        // $$('icon_pickfiles').onclick = function(ev) {
        //     uploader.setOption('toggle_btn', 'icon');
        // };

        $$('cover_pickfiles').onclick = function(ev) {
            uploader.setOption('toggle_btn', 'cover');
        };

        var tBox = document.querySelector('.drop-box');
        //目标box绑定dnd事件
        tarBoxBind(tBox);
        //取消document dnd事件
        retarBoxBind(document);

        function tarBoxBind(tBox) {
            tBox.ondragover = function(ev) {
                console.log('over');
                ev.preventDefault();
            }
            tBox.ondragenter = function(ev) {
                console.log('enter');
            }
            tBox.ondrop = function(ev) {
                console.log('ondrop')
                uploader.setOption('toggle_btn', 'drop');
            }
            tBox.ondragleave = function(ev) {
                console.log('dleave');
            }
        }

        function retarBoxBind(tBox) {
            tBox.ondragover = function(ev) {
                ev.preventDefault();
            }
            tBox.ondragenter = function(ev) {
                ev.preventDefault();
            }

            tBox.ondrop = function(ev) {
                ev.preventDefault();
            }
            tBox.ondragleave = function(ev) {
                ev.preventDefault();
            }
        }

        var f_name = '';
        var file_type = [];

        var uploader = Qiniu.uploader({
            runtimes: 'html5,flash,html4',
            browse_button: ['icon_pickfiles', 'cover_pickfiles'], //上传按钮的ID
            container: 'btn-uploader', //上传按钮的上级元素ID
            drop_element: 'text-input',
            max_file_size: '8m', //最大文件限制
            flash_swf_url: '/static/js/plupload/Moxie.swf',
            dragdrop: true,
            // chunk_size: '512kb', //分块大小
            uptoken_func: function() {
                console.log(file_type);
                var type = '.' + file_type.shift();
                //请求构造
                var q_url = 'http://106.75.137.2:8888/api/admin/pedia/up_token/?filename=';
                f_name = generateMixed(64) + type;
                q_url = q_url + f_name;
                var uptoken = '';
                var xhr = createXHR();
                /**
                 * callback
                 * @param  {[type]} ) {                       respon [description]
                 * @return {[type]}   [description]
                 */
                xhrthen(xhr, function() {
                    respon = JSON.parse(xhr.responseText);
                    console.log(respon)
                    uptoken = respon.up_token;
                });

                xhr.open('get', q_url, false);
                xhr.send();
                console.log(f_name)
                console.log(uptoken)
                return uptoken;
            },
            //设置请求qiniu-token的url
            //Ajax请求upToken的Url，**强烈建议设置**（服务端提供）
            // uptoken : '<Your upload token>',
            //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
            // unique_names: true,
            // 默认 false，key为文件名。若开启该选项，SDK会为每个文件自动生成key（文件名）
            // save_key: true,
            // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK在前端将不对key进行任何处理
            domain: 'http://o8covea2w.bkt.clouddn.com/', //自己的七牛云存储空间域名
            //multi_selection: true, //是否允许同时选择多文件
            //文件类型过滤，这里限制为图片类型
            filters: {
                mime_types: [{
                    title: "Image files",
                    extensions: "jpg,jpeg,gif,png"
                }]
            },
            get_new_uptoken: true,
            //每次get新的token
            auto_start: true,
            resize: {
                width: 512,
                height: 384,
                crop: true,
                quality: 100,
                preserve_headers: false
            },
            init: {
                //init_file该事件会在每一个文件被添加到上传队列前触发
                'FileFiltered': function(up, file) {
                    loading.show();
                    progress.show();
                    console.log('file_init');
                    console.log(file.size / 1024);

                    var type = file.type.replace(/image\//g, '');
                    console.log(type)
                    file_type.push(type);
                    // var fsize = Math.floor(file.size / 1024);
                    // up.stop();
                    // console.log('waiting');
                    // if (fsize >= 0) {
                    //     //基准宽
                    //     var base_w = 512;

                    //     var render = new Filerender();
                    //     render.readAsDataURL(file.getNative());
                    //     render.onload = (function(e) {
                    //         var image = new Image();
                    //         image.src = e.target.result;
                    //         image.onload = function() {
                    //             var scale = this.width / this.height;
                    //             var autoh = get_autoh(base_w, scale);

                    //             resize_img(base_w, autoh);

                    //             up.addFile(image)
                    //         };
                    //     });
                    //     render.onloadend = function(e) {
                    //         console.log('loadend')

                    //     };

                    // } else {
                    //     console.log('can_not_Resize')
                    // }

                    // function resize_img(width, height) {
                    //     var resize = {
                    //         width: Math.floor(width),
                    //         height: Math.floor(height),
                    //         crop: false,
                    //         quality: 90,
                    //         preserve_headers: true
                    //     }
                    //     up.setOption('resize', resize);
                    // }

                    // function get_autoh(width, scale) {
                    //     return width / scale;
                    // }
                },
                'FilesAdded': function(up, files) {
                    console.log('f_add')
                    console.log(files)
                },
                'FilesRemoved': function(up, files) {
                    console.log('remove')
                    console.log(files)
                },
                'BeforeUpload': function(up, file) {
                    console.log('upload_Before');
                },
                'UploadFile': function(up, file) {
                    console.log('upFile');
                    console.log(up.getOption('resize'));
                },
                'UploadProgress': function(up, file) {
                    console.log(file.size / 1024);
                    display_progress();

                    //可以在这里控制上传进度的显示
                    //可参考七牛的例子
                    function display_progress() {
                        console.log(uploader.total)
                        console.log($scope.upload_pc)
                        $scope.upload_size = Math.floor(uploader.total.size / 1024);
                        $scope.upload_ed = Math.floor(uploader.total.loaded / 1024);
                        $scope.upload_pc = uploader.total.percent;
                        $scope.upload_byte = Math.floor(uploader.total.bytesPerSec / 1024);
                        $scope.$digest()
                    }

                },
                'UploadComplete': function() {
                    loading.hide();
                    progress.hide();
                    file_type = [];
                },
                'FileUploaded': function(up, file, info) {
                    //每个文件上传成功后,处理相关的事情
                    //其中 info 是文件上传成功后，服务端返回的json，形式如
                    //{
                    //  "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
                    //  "key": "gogopher.jpg"
                    //}
                    /**
                     * 图片上传策略
                     */
                    console.log(info)
                    var domain = up.getOption('domain');
                    var res = eval('(' + info + ')');
                    var sourceLink = domain + res.key; //获取上传文件的链接地址
                    console.log(sourceLink);
                    /**
                     * 分发拖拽上传&点击上传
                     * @param  {[type]} uploader.getOption('isdrop') [description]
                     * @return {[type]}                              [description]
                     */
                    var toggle_btn = uploader.getOption('toggle_btn');
                    //三种渲染状态分发
                    switch (toggle_btn) {
                        case 'icon':
                            console.log('icon_upload')
                            md.set('pushpin', sourceLink);
                            md.render_img('icon_box', sourceLink);
                            break;
                        case 'cover':
                            console.log('cover_upload')
                            md.set('coverurl', sourceLink);
                            md.render_img('cover_box', sourceLink);
                            break;
                        case 'drop':
                            console.log('drop_upload')
                            var textarea = document.getElementById('text-input');
                            var img_tem = "![Alt text]( " + sourceLink + " '在编辑提示文字')";
                            var value = textarea.value;
                            value += img_tem;
                            textarea.value = value;
                            break;
                    }

                },
                'Error': function(up, err, errTip) {
                    console.log(err)
                    alert(errTip);
                },
                'Key': function(up, file) {
                    var key = f_name;
                    return key
                }
            }
        });

    })();
}

/**
 * imarker
 * @param {[type]} mapObj [description]
 */
function Imarker(mapObj, $http, md) {
    Ctrlbtn.call(this);

    var lock_btn = this.lock_btn;
    var unlock_btn = this.unlock_btn;
    var unlock = this.unlock;
    var lock = this.lock;

    var markers = [],
        marker = undefined,
        listers = [],
        isEdit = false,
        lng = undefined,
        lat = undefined;

    this.set_pos = function(mlng, mlat) {
        lng = mlng;
        lat = mlat;
    }
    this.get_pos = function() {
        return {
            lng: lng,
            lat: lat
        }
    }
    this.set_isEdit = function(bool) {
        isEdit = bool;
    }
    this.get_isEdit = function() {
        return isEdit;
    }
    this.get_marker = function() {
        return marker;
    }

    this.add = function(lng, lat, callback) {
        if (lng && lat) {
            var marker = new AMap.Marker({
                position: [lng, lat],
                map: mapObj,
                extData: null
            });
            markers.push(marker);
            //新marker绑定必须事件
            ev_bind(markers.length - 1);
        } else {
            alert("请耐心等待地址查询结果")
        }
        callback();
    };

    function ev_bind(num) {
        var lister = AMap.event.addListener(markers[num], 'click', function(ev) {
            if (this.getExtData()) {
                md.set('pedia_id', this.getExtData());
            }
            //格式化地址
            console.log(this.getPosition());
            var lnglat = this.getPosition().lng + ',' + this.getPosition().lat;
            reAddressCode(lnglat);

            unlock(['edit_md', 'edit_del_f', 'edit_mov_f']);
            lock_btn('edit_add_f');

            marker = this;
        });
        listers.push(lister);
    };

    this.del = function(marker, callback) {
        var id = marker.getExtData();
        mconsole('id', id);
        /**
         * id的存在用以判断是否已进入过md编辑
         * @param  {[type]} id [description]
         * @return {[type]}    [description]
         */
        if (id) {
            $http({
                url: __API__._DelAtc_ + id,
                method: 'DELETE', //put更新,post添加
                data: '',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function(data, status, headers, config) {
                console.log(data);
                mapObj.remove(marker);
                alert("删除成功");
            }).error(function(data, header, config, status) {
                console.log(data);
                console.log(header);
                console.log(status);
                alert(textStatus + "请检查网络连接");
            });
        } else {
            mapObj.remove(marker);
        }
        callback();
    };
    this.remove = function(marker, callback) {
        marker.setDraggable(true);
        callback();
    };
    this.get_all = function() {
        $http({
            url: __API__._GetPediaFAll_,
            method: "GET",
            data: '',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function(data, status, headers, config) {
            var markers_msg = data.pedia;
            console.log(markers_msg);
            //渲染marker
            render(markers_msg)
        }).error(function(data, header, config, status) {
            console.log(data);
            console.log(header);
            console.log(status);
            alert(textStatus + "请检查网络连接")
        });
    };
    //进入编辑
    this.init_marker_lister = function() {
        for (var i = 0; i < markers.length; i++) {
            //事件绑定
            ev_bind(i)
        }
    };

    this.del_marker_lister = function() {
        for (var i = 0; i < listers.length; i++) {
            //移除监听
            AMap.event.removeListener(listers[i]);
        }
    };
    //正向地址编码
    this.addressCode = function(address) {
        var key = "f04042dbf08df4d6b9dd84837d6aa8f4";
        var url = "http://restapi.amap.com/v3/geocode/geo?address=" + address + "&output=JSON&key=" + key;
        var pos = 'asd';
        $http({
                url: url,
                method: "GET",
                data: '',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            })
            /**
             * position需要传入数组对象
             */
            .success(function(data, status, headers, config) {
                console.log(data.geocodes[0].location)
                var lnglat = data.geocodes[0].location;
                var position = lnglat;
                var zoomlevel = 10;
                lnglat = lnglat.split(',');

                if (isEdit) {
                    lock_btn('edit_add_f');
                }

                console.log(data.geocodes[0].level);
                /**
                 * 设置放大级别以及中心点
                 */
                mapObj.setZoomAndCenter(zoomlevel, lnglat);
                /**
                 * 填充入逆地址编码
                 */
                reAddressCode(position);

            }).error(function(data, header, config, status) {
                console.log(data);
                console.log(header);
                console.log(status);
                alert(textStatus + "请检查网络连接")
            });
    };
    //逆向地址编码
    this.reAddressCode = function(lnglat) {
        var key = "f04042dbf08df4d6b9dd84837d6aa8f4";
        var url = "http://restapi.amap.com/v3/geocode/regeo?output=json&location=" + lnglat + "&key=" + key + "&radius=1000&extensions=all";

        $http({
            url: url,
            method: "GET",
            data: '',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function(data, status, headers, config) {
            console.log(data)
            console.log(data.regeocode.formatted_address)
            var address = data.regeocode.formatted_address;
            /**
             * 将信息填入md表单
             * @type {[type]}
             */
            md.set('province', data.regeocode.addressComponent.province);
            md.set('city', data.regeocode.addressComponent.city);
            md.set('location', lnglat);
            console.log(md.get_form())

            if (address != "")
                $$('readdress').value = address;
            else $$('readdress').value = "所查坐标超出能力范围";

        }).error(function(data, header, config, status) {
            console.log(data);
            console.log(header);
            console.log(status);
            alert(textStatus + "请检查网络连接")
        });
    }

    this.render_addresss = function() {}
    this.render_readdress = function() {}

    var reAddressCode = this.reAddressCode;
    var addressCode = this.addressCode;

    function render(markers_msg) {
        for (var i = 0; i < markers_msg.length; i++) {
            //坐标结构化
            var lnglat = markers_msg[i].location.replace(/\(|\)/g, '').split(',');
            //init 标记
            var marker = new AMap.Marker({
                position: lnglat,
                zIndex: 101,
                title: markers_msg[i].title,
                topWhenMouseOver: true,
                draggable: true,
                raiseOnDrag: true, //拖拽效果
                clickable: true,
                map: mapObj,
                extData: markers_msg[i].pedia_id
            });
            //加载icon
            if (markers_msg[i].pushpin) {
                var icon = new AMap.Icon({
                    image: markers_msg[i].pushpin,
                    size: new AMap.Size(38, 52),
                    //控制图标大小
                    imageSize: new AMap.Size(38, 52)
                        //icon缩放
                });
                //仅当icon为图片时才修正偏移
                var offset = new AMap.Pixel(-20, -50); //icon偏移量
                marker.setIcon(icon);
                marker.setOffset(offset);
            }
            console.log(markers_msg[i]);
            markers.push(marker);
        };
    };

};

function AddressTransition() {
    //经纬度对象
    this.lnglat = undefined;
}
AddressTransition.prototype = {
    constructor: AddressTransition,
    get_lnglat: function() {
        if (typeof this.lnglat != 'undefined') {
            return this.lnglat;
        } else return console.log(typeof this.lnglat);
    },
    set_lnglat: function(pos) {
        var pos = pos.split(',');
        this.lnglat = pos;
    },
    //正向编码
    addressCode: function($http, address, callback) {
        var key = "f04042dbf08df4d6b9dd84837d6aa8f4";
        var url = "http://restapi.amap.com/v3/geocode/geo?address=" + address + "&output=JSON&key=" + key;
        var pos = 'asd';
        $http({
                url: url,
                method: "GET",
                data: '',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            })
            /**
             * position需要传入数组对象
             */
            .success(function(data, status, headers, config) {
                console.log(data.geocodes[0].location)
                var pos = data.geocodes[0].location;

                callback(pos);

            }).error(function(data, header, config, status) {
                console.log(data);
                console.log(header);
                console.log(status);
                alert(textStatus + "请检查网络连接")
            });
    },
    //逆向编码
    reAddressCode: function($http, lnglat, callback) {
        var key = "f04042dbf08df4d6b9dd84837d6aa8f4";
        var url = "http://restapi.amap.com/v3/geocode/regeo?output=json&location=" + lnglat + "&key=" + key + "&radius=1000&extensions=all";

        $http({
            url: url,
            method: "GET",
            data: '',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function(data) {
            console.log(data)
            console.log(data.regeocode.formatted_address)

            callback(data);

        }).error(function(data, header, config, status) {
            console.log(data);
            console.log(header);
            console.log(status);
            alert(textStatus + "请检查网络连接")
        });
    }
}

//操作控制面板
function Ctrlbtn() {

    this.lock_btn = function(id) {
        $$(id).setAttribute('disabled', 'disabled');
    };
    this.unlock_btn = function(id) {
        $$(id).removeAttribute('disabled');
    };
    this.lock = function(array) {
        for (var i = array.length - 1; i >= 0; i--) {
            $$(array[i]).setAttribute('disabled', 'disabled');
        }
    };
    this.unlock = function(array) {
        for (var i = array.length - 1; i >= 0; i--) {
            $$(array[i]).removeAttribute('disabled');
        }
    };
};

function ctrl_app_mager__plant_science($stateParams, $http, $scope, $state) {
    /**
     * 构建地图
     * @type {AMap}
     */
    var mapObj = new AMap.Map('container', {
        zoom: 5, //缩放级别
        center: [113.544184, 22.214936], //中心点
        isHotspot: true, //地图热点
        keyboardEnable: true, //键盘控制
        mapStyle: 'fresh' //normal（默认样式）、dark（深色样式）、light（浅色样式）、fresh(osm样式)
    });

    var address = new AddressTransition();

    address.addressCode($http, '佛山', function(pos) {
        console.log(pos)
        address.reAddressCode($http, pos, function(data) {
            console.log(data)
        })
    });


    /**
     * 工具条&比例尺
     * @param  {AMap}   ) {                           var toolBar [description]
     * @return {[type]}   [description]
     */
    AMap.plugin(['AMap.ToolBar', 'AMap.Scale'], function() {
        //TODO  创建控件并添加
        var toolBar = new AMap.ToolBar();
        var scale = new AMap.Scale();
        mapObj.addControl(toolBar);
        mapObj.addControl(scale);
    });

    //init
    var md = new EditForm();
    md.init({
        pedia_id: '',
        title: '',
        city: '',
        province: '',
        pushpin: '',
        category: '',
        context: '',
        location: '',
        markdown: '',
        coverurl: ''
    });
    var imarker = new Imarker(mapObj, $http, md);
    var ctrlbtn = new Ctrlbtn();
    imarker.get_all();


    //地图点击
    AMap.event.addListener(mapObj, 'click', function(ev) {
        if (imarker.get_isEdit()) {
            ctrlbtn.unlock_btn('edit_add_f');
        }
        ctrlbtn.lock(['edit_del_f', 'edit_mov_f', 'edit_md']);
        //设置坐标
        var lng = ev.lnglat.getLng();
        var lat = ev.lnglat.getLat();
        imarker.set_pos(lng, lat)

        var lnglat = lng + ',' + lat;
        $$('xy').value = lnglat;

        imarker.reAddressCode(lnglat);
    });
    //退出编辑
    $$('exit_edit').onclick = function() {
        console.log('exit_edit')
        imarker.set_isEdit(false);
        ctrlbtn.lock(['edit_md', 'edit_del_f', 'edit_add_f', 'exit_edit', 'edit_mov_f']);
        imarker.del_marker_lister();
    };
    //进入编辑
    $$('edit_map').onclick = function() {
        console.log('edit_map')
        imarker.set_isEdit(true);
        ctrlbtn.unlock_btn('exit_edit');
        imarker.init_marker_lister();
    };
    //正向地址转换
    $$('check_address').onclick = function() {
        var address = document.getElementById('addressCode').value;
        /**
         * 进行正向地址变换
         */
        imarker.addressCode(address);
        console.log(address)
    };

    //操作面板
    //
    //删除marker
    $$('edit_del_f').onclick = function() {
        var this_marker = imarker.get_marker();
        imarker.del(this_marker, function() {
            ctrlbtn.lock(['edit_md', 'edit_del_f', 'edit_mov_f'])
        });
    };
    //移动marker
    $$('edit_mov_f').onclick = function() {
        var this_marker = imarker.get_marker();
        imarker.remove(this_marker, function() {
            ctrlbtn.lock(['edit_md', 'edit_del_f', 'edit_mov_f'])
        });
    };
    //添加新marker
    $$('edit_add_f').onclick = function() {
        var pos = imarker.get_pos();
        console.log(pos)
        imarker.add(pos.lng, pos.lat, function() {
            ctrlbtn.lock_btn('edit_add_f')
        });

    };
    //进入md编辑
    $scope.enter_md = function() {
        console.log('enter_md');
        //传入必要参数
        if (md.get('province') && md.get('city') && md.get('location')) {
            $state.go('app_mager__gar_recom', {
                province: md.get('province'),
                city: md.get('city'),
                location: md.get('location'),
                pedia_id: md.get('pedia_id')
            });
        } else {
            alert('请检查网络连接_/或/_等待地址搜索结果...')
        }
    };


    /**
     * 对添加标记的锁
     * @type {Boolean}
     */
    // var isEdit = false;
    // var lng, lat;
    // var markers = [];
    // var listers = [];
    /**
     * 加载marker
     * @return {[type]} [description]
     */
    // (function() {
    //     $http({
    //         url: __API__._GetPediaFAll_,
    //         method: "GET",
    //         data: '',
    //         headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    //     }).success(function(data, status, headers, config) {
    //         var provinces = data.pedia;
    //         console.log(provinces);
    //         /**
    //          * 初始化marker
    //          */
    //         init_markers(provinces)
    //     }).error(function(XMLHttpRequest, textStatus, errorThrown) {
    //         console.log(XMLHttpRequest);
    //         console.log(XMLHttpRequest);
    //         console.log(textStatus);
    //         alert(textStatus + "请检查网络连接")
    //     });
    // })();

    /**
     * 需将具体坐标传回后台
     * 地图点击初始化
     * @param {[type]} lng [description]
     * @param {[type]} lat [description]
     */
    // var mapClick = AMap.event.addListener(mapObj, 'click', function(ev) {
    //     /**
    //      * 对锁的判定
    //      * @param  {[type]} isEdit [description]
    //      * @return {[type]}            [description]
    //      */
    //     if (isEdit) {
    //         alw_click('edit_add_f');
    //     }
    //     /**
    //      * 关闭编辑入口
    //      */
    //     dis_click('edit_md');
    //     dis_click('edit_del_f');
    //     dis_click('edit_mov_f');

    //     console.log('ok')
    //     lng = ev.lnglat.getLng();
    //     lat = ev.lnglat.getLat();
    //     var lnglat = lng + ',' + lat;
    //     $$('xy').value = lnglat;
    //     /**
    //      * 点击地图时进行逆地址编码
    //      */
    //     reAddressCode(lnglat);
    // });
    /**
     * 锁定工具栏
     */
    // var edit_del_f = document.querySelector('#edit_del_f')
    // var edit_mov_f = document.getElementById('edit_mov_f')
    // var edit_add_f = document.getElementById('edit_add_f')
    /**
     * 锁定编辑操作
     * @return {[type]} [description]
     */
    // document.getElementById('exit_edit').onclick = function() {
    //     edit_del_f.onclick = null;
    //     isEdit = false;
    //     edit_mov_f.onclick = null;
    //     dis_click('edit_md');
    //     dis_click('edit_del_f');
    //     dis_click('edit_mov_f');
    //     dis_click('edit_add_f');
    //     dis_click('exit_edit');
    //     dis_f_lister();
    // };
    /**
     * 编辑地图操作
     * @return {[type]} [description]
     */
    // document.getElementById('edit_map').onclick = function() {
    //     alw_click('exit_edit');
    //     add_f_lister();
    // };
    /**
     * 绑定地址正向编码事件
     * @return {[type]} [description]
     */
    // document.getElementById('check_address').onclick = function() {
    //     var address = document.getElementById('addressCode').value;
    //     /**
    //      * 进行正向地址变换
    //      */
    //     addressCode(address);
    //     console.log(address)
    // };





    /**
     * 增加标记点监听事件
     * 进入编辑操作
     */
    //     function add_f_lister() {
    //         /**
    //          * 添加标记的锁
    //          * @type {Boolean}
    //          */
    //         isEdit = true;
    //         /**
    //          * 对标记添加事件
    //          */
    //         for (var i = 0; i < markers.length; i++) {
    //             var lister = AMap.event.addListener(markers[i], 'click', function(ev) {
    //                 /**
    //                  * 获取marker_id
    //                  * @type {[type]}
    //                  */
    //                 md.set('pedia_id', this.getExtData());
    //                 /**
    //                  * 关闭添加标记入口
    //                  */
    //                 dis_click('edit_add_f');
    //                 /**
    //                  * 查询结构化地址
    //                  */
    //                 // console.log(this.id)
    //                 console.log(this.getPosition());
    //                 var lnglat = this.getPosition().lng + ',' + this.getPosition().lat;
    //                 reAddressCode(lnglat);
    //                 /**
    //                  * 解锁工具栏
    //                  */
    //                 re_t_lock();
    //                 /**
    //                  * 监听工具栏
    //                  * @return {[type]} [description]
    //                  */
    //                 var this_marker = this;
    //                 /**
    //                  * btn添加删除
    //                  * @return {[type]} [description]
    //                  */
    //                 edit_del_f.onclick = function() {
    //                     del_f(this_marker, t_lock);
    //                 };
    //                 /**
    //                  * btn添加移动
    //                  * @return {[type]} [description]
    //                  */
    //                 edit_mov_f.onclick = function() {
    //                     mov_f(this_marker, t_lock);
    //                 };
    //                 /**
    //                  * btn添加md编辑
    //                  * @return {[type]} [description]
    //                  */
    //                 edit_md.onclick = function() {
    //                     enter_md(this_marker, t_lock);
    //                 };
    //             });
    //             listers.push(lister);
    //         }
    //         /**
    //          * btn新增
    //          * 需获取地图点击事件中的坐标
    //          * @return {[type]} [description]
    //          */
    //         edit_add_f.onclick = function() {
    //             console.log(lng + lat)
    //             if (lng && lat) {
    //                 add_f(lng, lat, function() {
    //                     dis_click('edit_add_f');
    //                 });
    //             }
    //         };
    //     }

    //     /**
    //      * 初始化标记
    //      * @type {AMap}
    //      */
    //     function init_markers(provinces) {
    //         for (var i = 0; i < provinces.length; i++) {

    //             if (provinces[i].pushpin) {
    //                 var icon = new AMap.Icon({
    //                     image: provinces[i].pushpin,
    //                     size: new AMap.Size(38, 52),
    //                     //控制图标大小
    //                     imageSize: new AMap.Size(38, 52)
    //                         //icon缩放
    //                 });
    //                 // console.log(icon)
    //             } else {
    //                 var icon = null;
    //             }
    //             console.log(provinces[i].pedia_id);
    //             /**
    //              * 结构化坐标
    //              * @type {[type]}
    //              */
    //             var lnglat = provinces[i].location.replace(/\(|\)/g, '').split(',');
    //             /**
    //              * 生成标记（覆盖物）
    //              * @type {AMap}
    //              */
    //             marker = new AMap.Marker({
    //                 position: lnglat,
    //                 zIndex: 101,
    //                 title: provinces[i].title,
    //                 topWhenMouseOver: true,
    //                 draggable: true,
    //                 raiseOnDrag: true, //拖拽效果
    //                 clickable: true,
    //                 map: mapObj,
    //                 extData: provinces[i].pedia_id
    //             });
    //             if (icon) {
    //                 var offset = new AMap.Pixel(-20, -50); //icon偏移量
    //                 marker.setIcon(icon);
    //                 marker.setOffset(offset);
    //             }
    //             markers.push(marker);
    //         }
    //     };

    //     /**
    //      * 撤销标记点击事件
    //      * @return {[type]} [description]
    //      */
    //     function dis_f_lister() {
    //         for (var i = 0; i < listers.length; i++) {
    //             AMap.event.removeListener(listers[i]);
    //         }
    //     }


    //     /**
    //      * 锁定工具栏
    //      * @return {[type]} [description]
    //      */
    //     function t_lock() {
    //         dis_click('edit_md');
    //         dis_click('edit_del_f');
    //         dis_click('edit_mov_f');
    //     }

    //     /**
    //      * 解锁
    //      * @return {[type]} [description]
    //      */
    //     function re_t_lock() {
    //         alw_click('edit_md');
    //         alw_click('edit_del_f');
    //         alw_click('edit_mov_f');
    //     }

    //     function dis_click(id) {
    //         document.getElementById(id).setAttribute('disabled', 'disabled');
    //     }

    //     function alw_click(id) {
    //         document.getElementById(id).removeAttribute('disabled');
    //     }

    //     /**
    //      * 添加标记
    //      * @param {[type]}   lng      [description]
    //      * @param {[type]}   lat      [description]
    //      * @param {Function} callback [description]
    //      */
    //     function add_f(lng, lat, callback) {
    //         var marker = new AMap.Marker({
    //             position: [lng, lat],
    //             map: mapObj,
    //             extData: null
    //         });
    //         // marker.setE
    //         markers.push(marker);
    //         /**
    //          * 添加事件
    //          */
    //         new_f_ev();
    //         callback();
    //     }

    //     function new_f_ev() {
    //         /**
    //          * 为新增加的标记添加事件
    //          */
    //         var lister = AMap.event.addListener(markers[markers.length - 1], 'click', function(ev) {
    //             /**
    //              * 解锁工具栏
    //              */
    //             re_t_lock();
    //             dis_click('edit_add_f');
    //             /**
    //              * 监听工具栏
    //              * @return {[type]} [description]
    //              */
    //             var this_marker = this;

    //             edit_del_f.onclick = function() {
    //                 del_f(this_marker, t_lock);
    //             };
    //             edit_mov_f.onclick = function() {
    //                 mov_f(this_marker, t_lock);
    //             };
    //         });
    //         listers.push(lister);
    //     }

    //     /**
    //      * 删除标记
    //      * @param  {[type]}   marker   [description]
    //      * @param  {Function} callback [description]
    //      * @return {[type]}            [description]
    //      */
    //     function del_f(marker, callback) {
    //         var id = marker.getExtData();
    //         mconsole('id', id);
    //         /**
    //          * id的存在用以判断是否已进入过md编辑
    //          * @param  {[type]} id [description]
    //          * @return {[type]}    [description]
    //          */
    //         if (id) {
    //             $http({
    //                 url: __API__._DelAtc_ + id,
    //                 method: 'DELETE', //put更新,post添加
    //                 data: '',
    //                 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    //             }).success(function(data, status, headers, config) {
    //                 console.log(data)
    //                 mapObj.remove(marker);
    //             }).error(function(XMLHttpRequest, textStatus, errorThrown) {
    //                 console.log(XMLHttpRequest);
    //                 console.log(XMLHttpRequest);
    //                 console.log(textStatus);
    //                 alert(textStatus + "请检查网络连接")
    //             });
    //         } else {
    //             mapObj.remove(marker);
    //         }
    //         callback();
    //     }

    //     /**
    //      * 移动标记
    //      * @param  {[type]}   this_marker [description]
    //      * @param  {Function} callback    [description]
    //      * @return {[type]}               [description]
    //      */
    //     function mov_f(this_marker, callback) {
    //         this_marker.setDraggable(true);
    //         callback();
    //     }

    //     /**
    //      * 进入md编辑
    //      * @param  {[type]} this_marker [description]
    //      * @param  {[type]} t_lock      [description]
    //      * @return {[type]}             [description]
    //      */
    //     function enter_md(this_marker, t_lock) {

    //     }

    //     /**
    //      * 正向地址编码
    //      * @param  {[type]} address [description]
    //      * @return {[type]}         [description]
    //      */
    //     function addressCode(address) {
    //         var key = "f04042dbf08df4d6b9dd84837d6aa8f4";
    //         var url = "http://restapi.amap.com/v3/geocode/geo?address=" + address + "&output=JSON&key=" + key;
    //         var pos = 'asd';
    //         $http({
    //                 url: url,
    //                 method: "GET",
    //                 data: '',
    //                 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    //             })
    //             /**
    //              * position需要传入数组对象
    //              * @param  {[type]} data    [description]
    //              * @param  {[type]} status  [description]
    //              * @param  {[type]} headers [description]
    //              * @param  {[type]} config) {                       console.log(data.geocodes[0].location)            var lnglat [description]
    //              * @return {[type]}         [description]
    //              */
    //             .success(function(data, status, headers, config) {
    //                 console.log(data.geocodes[0].location)
    //                 var lnglat = data.geocodes[0].location;
    //                 var position = lnglat;
    //                 var zoomlevel = 10;
    //                 lnglat = lnglat.split(',');

    //                 lng = lnglat[0];
    //                 lat = lnglat[1];

    //                 if (isEdit) {
    //                     alw_click('edit_add_f');
    //                 }

    //                 console.log(data.geocodes[0].level);
    //                 /**
    //                  * 设置放大级别以及中心点
    //                  */
    //                 mapObj.setZoomAndCenter(zoomlevel, lnglat);
    //                 /**
    //                  * 填充入逆地址编码
    //                  */
    //                 reAddressCode(position);


    //             }).error(function(XMLHttpRequest, textStatus, errorThrown) {
    //                 console.log(XMLHttpRequest.status);
    //                 console.log(XMLHttpRequest.readyState);
    //                 console.log(textStatus);
    //                 alert(textStatus + "请检查网络连接")
    //             });
    //     }

    //     /**
    //      * http://restapi.amap.com/v3/geocode/regeo?output=xml&location=116.310003,39.991957&key=<用户的key>&radius=1000&extensions=all
    //      * http逆地址编码服务
    //      */
    //     function reAddressCode(lnglat) {
    //         var key = "f04042dbf08df4d6b9dd84837d6aa8f4";
    //         var url = "http://restapi.amap.com/v3/geocode/regeo?output=json&location=" + lnglat + "&key=" + key + "&radius=1000&extensions=all";

    //         $http({
    //             url: url,
    //             method: "GET",
    //             data: '',
    //             headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    //         }).success(function(data, status, headers, config) {
    //             console.log(data)
    //             console.log(data.regeocode.formatted_address)
    //             var address = data.regeocode.formatted_address;
    //             /**
    //              * 将信息填入md表单
    //              * @type {[type]}
    //              */
    //             md.set('province', data.regeocode.addressComponent.province);
    //             md.set('city', data.regeocode.addressComponent.city);
    //             md.set('location', lnglat);
    //             console.log(md.get_form())

    //             if (address != "")
    //                 document.getElementById('readdress').value = address;
    //             else document.getElementById('readdress').value = "所查坐标超出能力范围";

    //         }).error(function(XMLHttpRequest, textStatus, errorThrown) {
    //             console.log(XMLHttpRequest.status);
    //             console.log(XMLHttpRequest.readyState);
    //             console.log(textStatus);
    //             alert(textStatus + "请检查网络连接")
    //         });
    //     }


}
