var activity_farmer_test = {
    "isManagerPlant": false,
    "greens": [{
        "name": "大白菜",
        "id": "1",
        "matureTime": "30天",
        "isSelected": false
    }, {
        "name": "芥蓝",
        "id": "2",
        "matureTime": "40天",
        "isSelected": false
    }, {
        "name": "通心菜",
        "id": "3",
        "matureTime": "50天",
        "isSelected": false
    }, {
        "name": "小白菜",
        "id": "4",
        "matureTime": "30天",
        "isSelected": false
    }, {
        "name": "油菜心",
        "id": "5",
        "matureTime": "20天",
        "isSelected": false
    }]
};
/**
 * 活动-农场主
 * @param  {$scope} $scope     [description]
 * @param  {$http} $http      [description]
 * @param  {Page} Page       设置title
 * @param  {$rootScope} $rootScope [description]
 */
function ctrl_activity_farmer($scope, $http, Page, $rootScope, Token, $state) {
    //init-calendar
    // $(function() {
    //     $('#beginTime').date();
    // });

    //日历控件
    $(function() {
        $('#dd').calendar({
            trigger: '#dt',
            zIndex: 999,
            format: 'yyyy-mm-dd',
            onSelected: function(view, date, data) {
                console.log('event: onSelected')
            },
            onClose: function(view, date, data) {
                console.log('event: onClose')
                console.log('view:' + view)
                console.log('date:' + date)
                console.log('data:' + (data || 'None'));
            }
        });
    });
    //config
    Page.setTitle('招募农场主');

    //获取商品信息
    var xhr = new Jsonp($http);

    get_farm_msg();

    Token.get(function(data) {
        get_farm_msg();
    });

    function get_farm_msg() {
        xhr.get(_SERVER_ + "/farmGet?access_token=" + _ACCESSTOKEN_ + "&callback=JSON_CALLBACK", function(data) {
            log(data);
            $scope.isLoading = false;
            if (data.result == 2) {
                $scope.isStorage = false;
            } else {
                $scope.greens = data.crops;
                $scope.farm = data.farmConstantModel;
                $scope.isStorage = true;
            }
        });
    }
    /**
     * toast init
     * @type {Object}
     */
    $scope.toastdata = {
        title: 'Toast 调试',
        content: '不必紧张',
        hint: 'zxc',
        show: false
    };
    /**
     * hint init
     * @type {Object}
     */
    $scope.hintdate = {
        content: '最多只能选3种作物喔~',
        show: false
    };

    function hint_delay(content) {
        $scope.hintdate = {
            content: content,
            show: false
        };
        hint.delay(4000);
    }

    var cost = new Cost($scope, 'costdata');
    $scope.costdata = {
        cost: 0,
        btn_centent: '下一步',
        next: function() {
            var plan_time = $$('dt').value;
            log(plan_time, 'PT')
            if ($scope.isStorage) {
                if (plan_time || !$scope.isGoPlant) { //可选择不前往农场
                    if (compare_date(now_date(), plan_time) || !$scope.isGoPlant) {
                        if (!arr_empty(greens_id)) { //可选择自己管理农场
                            if (isMax()) { //选择适当数量的蔬果
                                //format init
                                if ($scope.isGoPlant) {
                                    sessionStorage.setItem("sow", "0");
                                } else sessionStorage.setItem("sow", "1");
                                if ($scope.isManagerPlant) {
                                    sessionStorage.setItem("manageCrops", "1");
                                } else sessionStorage.setItem("manageCrops", "-1");
                                sessionStorage.setItem("greens_id", '[' + greens_id.join(',') + ']');
                                if (plan_time) {
                                    var timestamp = Date.parse(plan_time);
                                } else var timestamp = 0;
                                log(timestamp, 'time')
                                $state.go('activity.pay', {
                                    couponGroupIds: '[' + greens_id.join(',') + ']',
                                    cost: cost.get(),
                                    planTime: timestamp,
                                    url: "http://wx.imlenong.com/pay/pay_farm.html"
                                });
                            } else hint_delay("请选择3种蔬果种子");
                        } else hint_delay("请选择蔬菜种子");
                    } else hint_delay("请选择正确日期");
                } else hint_delay("请选择日期");
            } else hint_delay("土地被抢光了");
        }
    };

    // 弹出层管理器
    var dialog = new Popup($scope, 'toastdata');
    var hint = new Popup($scope, 'hintdate');


    $scope.isAllSelect = false; //显示成熟时间表
    $scope.isManagerPlant = true; //是否管理农场
    $scope.isGoPlant = true; //是否前往农场
    $scope.isMax = false; //作物是否达到最大值
    $scope.isStorage = false;
    $scope.isLoading = true;

    var greens_id = []; //维护蔬菜
    // $scope.data_list = activity_farmer_test; //test date demo

    $scope.manager_farm = function(bool) {
        if (bool) {
            $scope.isManagerPlant = true;
        } else $scope.isManagerPlant = false;
        cost_price(greens_id, $scope.isManagerPlant, function(data) {
            if (data.result == '1') {
                cost.set(data.price);
            } else cost.set("0");
        });
    };
    //数据更新时检查，并检查非全选状态
    $scope.check_data = function(isSelect) {
        // console.log(isSelect)
        //是否超过最大种植数
        if (isMax()) {
            $scope.isMax = true;
        } else $scope.isMax = false;
        log($scope.isMax);

        if (!cont()) {
            $scope.isAllSelect = false;
        } else $scope.isAllSelect = true;
    };
    //改变复选框时
    $scope.change_select = function(data, index) {
        var isSelect = data.isSelected;
        var id = data.id;
        // log($scope.isManagerPlant, '--')
        if (isSelect) {
            $scope.greens[index].isSelected = false;
            remove(greens_id, id);
        } else {
            $scope.greens[index].isSelected = true;
            greens_id.push(id);
            console.log(greens_id);
        }
        cost_price(greens_id, $scope.isManagerPlant, function(data) {
            if (data.result == '1') {
                cost.set(data.price);
            } else cost.set("0");
        });
        // log(data.isSelected, 'isSelect');
        //是否超过最大种植数
        if (isMax()) {
            hint_delay("小农场只能种下3种蔬果~");
        }
        cost_greens_price();
    };

    function cost_greens_price() {
        var greens = $scope.greens;
        var max = 0;
        for (var i = greens.length - 1; i >= 0; i--) {
            // log(greens[i].isSelected, 'GP');
            if (greens[i].isSelected) {
                if (max < greens[i].price) {
                    max = greens[i].price;
                }
            }
        }
        log(max, 'GP');
        // $scope.greens_price = max;
        $scope.greens_price = 80;
    }

    function cost_price(greens_id, manageCrops, callback) {
        greens_id = '[' + greens_id.join(',') + ']';

        if (manageCrops) {
            manageCrops = '1';
        } else manageCrops = '-1';
        log(manageCrops, 'MC')
        var get_price_url = _SERVER_ + "/farmAddPrice?access_token=" + _ACCESSTOKEN_ + "&crops=" + encodeURIComponent(greens_id) + "&manageCrops=" + manageCrops + "&callback=JSON_CALLBACK";
        log(get_price_url);
        xhr.get(get_price_url, function(data) {
            log(data);
            callback(data);
        });
    }

    /**
     * 选择前往农庄时
     */
    $scope.goPlant = function() {
        if ($scope.isGoPlant) {
            $scope.isGoPlant = false;
        } else $scope.isGoPlant = true;
    };
    /**
     * 所选农作物计数器
     * @return {int} [description]
     */
    function cont() {
        var greens = $scope.greens;
        var con = 0;
        for (var i = greens.length - 1; i >= 0; i--) {
            if (greens[i].isSelected) {
                con++;
            }
        }
        return con;
    }
    /**
     * 最大值判断
     * @return {Boolean} [description]
     */
    function isMax() {
        var _PLANT_NUM_ = 3;
        //检查作物选择数
        if (cont() >= _PLANT_NUM_) {
            return true;
        } else return false;
    }
    /**
     * 判断空数组
     * @param  {arr} arr [description]
     * @return {bool}     [description]
     */
    function arr_empty(arr) {
        if (arr.length == 0)
            return true;
        else return false;
    }
    /**
     * 移除指定的数组项
     * @param  {arr} arr [description]
     * @param  {val} val [description]
     * @return {}     [description]
     */
    function remove(arr, val) {
        var index = arr.indexOf(val);
        if (index > -1) {
            arr.splice(index, 1);
        }
    }
    /**
     * 获取最新日期
     * @return {date} yyyy-mm-dd
     */
    function now_date() {
        var now = new Date(),
            year = now.getFullYear(),
            month = now.getMonth() + 1,
            day = now.getDate();
        var date = [year, month, day];
        var format = date.join("-");
        return format;
    }

    //比较日前大小  
    function compare_date(checkStartDate, checkEndDate) {
        var arys1 = new Array();
        var arys2 = new Array();
        if (checkStartDate != null && checkEndDate != null) {
            arys1 = checkStartDate.split('-');
            var sdate = new Date(arys1[0], parseInt(arys1[1] - 1), arys1[2]);
            arys2 = checkEndDate.split('-');
            var edate = new Date(arys2[0], parseInt(arys2[1] - 1), arys2[2]);
            if (sdate > edate) {
                log("date refuse");
                return false;
            } else {
                log("date ok");
                return true;
            }
        }
    }
}

function ctrl_activity_seed($scope, $http, Page, Token, $state, $stateParams) {
    //日历控件
    $(function() {
        $('#dd').calendar({
            trigger: '#dt',
            zIndex: 999,
            format: 'yyyy-mm-dd',
            onSelected: function(view, date, data) {
                console.log('event: onSelected')
            },
            onClose: function(view, date, data) {
                console.log('event: onClose')
                console.log('view:' + view)
                console.log('date:' + date)
                console.log('data:' + (data || 'None'));
            }
        });
    });
    //config
    Page.setTitle('购买种子');

    //获取商品信息
    var xhr = new Jsonp($http);

    get_seed_msg();

    Token.get(function(data) {
        get_seed_msg();
    });

    function get_seed_msg() {
        xhr.get(_SERVER_ + "/cropGet?access_token=" + _ACCESSTOKEN_ + "&callback=JSON_CALLBACK", function(data) {
            log(data);
            $scope.isLoading = false;
            if (data.result == 2) {
                $scope.isStorage = false;
            } else {
                $scope.greens = data.crops;
                $scope.farm = data.farmConstantModel;
            }
        });
    }
    /**
     * toast init
     * @type {Object}
     */
    $scope.toastdata = {
        title: 'Toast 调试',
        content: '不必紧张',
        hint: 'zxc',
        show: false
    };
    /**
     * hint init
     * @type {Object}
     */
    $scope.hintdate = {
        content: '土地只能种下这么多的蔬菜了~',
        show: false
    };

    function hint_delay(content) {
        $scope.hintdate = {
            content: content,
            show: false
        };
        hint.delay(4000);
    }

    var cost = new Cost($scope, 'costdata');
    $scope.costdata = {
        cost: 0,
        btn_centent: '下一步',
        next: function() {
            var plan_time = $$('dt').value;

            if (plan_time || !$scope.isGoPlant) { //可选择不前往农场
                if (compare_date(now_date(), plan_time) || !$scope.isGoPlant) {
                    if (!arr_empty(greens_id)) { //可选择自己管理农场
                        //format init
                        if ($scope.isGoPlant) {
                            sessionStorage.setItem("sow", "0");
                        } else sessionStorage.setItem("sow", "1");
                        if ($scope.isManagerPlant) {
                            sessionStorage.setItem("manageCrops", "1");
                        } else sessionStorage.setItem("manageCrops", "-1");
                        sessionStorage.setItem("greens_id", '[' + greens_id.join(',') + ']');
                        if (plan_time) {
                            var timestamp = Date.parse(plan_time);
                            sessionStorage.setItem("time_go_farm", timestamp);
                        } else sessionStorage.setItem("time_go_farm", '0');
                        sessionStorage.setItem("myFarmId", farm_id);
                        // $state.go('activity.pay', {
                        //     couponGroupIds: '[' + greens_id.join(',') + ']',
                        //     cost: cost.get(),
                        //     planTime: timestamp,
                        //     url: 
                        // });
                        location.href = "http://wx.imlenong.com/pay/pay_seed.html";
                    } else hint_delay("请选择蔬菜种子");
                } else hint_delay("请选择正确的日期");
            } else hint_delay("请选择日期");

        }
    };

    // 弹出层管理器
    var dialog = new Popup($scope, 'toastdata');
    var hint = new Popup($scope, 'hintdate');

    $scope.isAllSelect = false; //显示成熟时间表
    $scope.isManagerPlant = true; //是否管理农场
    $scope.isGoPlant = true; //是否前往农场
    $scope.isMax = false; //作物是否达到最大值
    $scope.isStorage = false;
    $scope.isLoading = true;
    var greens_limit = $stateParams.buyCrop;
    var farm_id = $stateParams.farmId;

    var greens_id = []; //维护蔬菜
    // $scope.data_list = activity_farmer_test; //test date demo

    $scope.manager_farm = function(bool) {
            if (bool) {
                $scope.isManagerPlant = true;
            } else $scope.isManagerPlant = false;
            cost_price(greens_id, $scope.isManagerPlant, function(data) {
                if (data.result == '1') {
                    cost.set(data.price);
                } else cost.set("0");
            });
        }
        //数据更新时检查，并检查非全选状态
    $scope.check_data = function(isSelect) {
        // console.log(isSelect)
        //是否超过最大种植数
        if (isMax()) {
            $scope.isMax = true;
        } else $scope.isMax = false;
        log($scope.isMax);

        if (!cont()) {
            $scope.isAllSelect = false;
        } else $scope.isAllSelect = true;
    };
    //改变复选框时
    $scope.change_select = function(data, index) {
        var isSelect = data.isSelected;
        var id = data.id;
        if (isSelect) {
            $scope.greens[index].isSelected = false;
            remove(greens_id, id);
        } else {
            $scope.greens[index].isSelected = true;
            greens_id.push(id);
            console.log(greens_id);
        }
        cost_price(greens_id, $scope.isManagerPlant, $stateParams.farmId, function(data) {
            if (data.result == '1') {
                cost.set(data.price);
            } else cost.set("0");
        });
        log(data.isSelected, 'isSelect');
        //是否超过最大种植数
        if (isMax()) {
            hint.delay(4000);
        }
    };

    function cost_price(greens_id, manageCrops, myFarmId, callback) {
        greens_id = '[' + greens_id.join(',') + ']';
        if (manageCrops) {
            manageCrops = '1';
        } else manageCrops = '-1';
        log(manageCrops, 'MC')
        var get_price_url = _SERVER_ + "/cropAddPrice?access_token=" + _ACCESSTOKEN_ + "&crops=" + encodeURIComponent(greens_id) + "&manageCrops=" + manageCrops + "&myFarmId=" + myFarmId + "&callback=JSON_CALLBACK";
        log(get_price_url);
        xhr.get(get_price_url, function(data) {
            log(data);
            callback(data);
        });
    }

    /**
     * 选择前往农庄时
     */
    $scope.goPlant = function() {
        if ($scope.isGoPlant) {
            $scope.isGoPlant = false;
        } else $scope.isGoPlant = true;
    };
    /**
     * 所选农作物计数器
     * @return {int} [description]
     */
    function cont() {
        var greens = $scope.greens;
        var con = 0;
        for (var i = greens.length - 1; i >= 0; i--) {
            if (greens[i].isSelected) {
                con++;
            }
        }
        return con;
    }
    /**
     * 最大值判断
     * @return {Boolean} [description]
     */
    function isMax() {
        var _PLANT_NUM_ = greens_limit;
        // var _PLANT_NUM_ = 3;
        //检查作物选择数
        if (cont() >= _PLANT_NUM_) {
            return true;
        } else return false;
    }
    /**
     * 判断空数组
     * @param  {arr} arr [description]
     * @return {bool}     [description]
     */
    function arr_empty(arr) {
        if (arr.length == 0)
            return true;
        else return false;
    }
    /**
     * 移除指定的数组项
     * @param  {arr} arr [description]
     * @param  {val} val [description]
     * @return {}     [description]
     */
    function remove(arr, val) {
        var index = arr.indexOf(val);
        if (index > -1) {
            arr.splice(index, 1);
        }
    }
    /**
     * 获取最新日期
     * @return {date} yyyy-mm-dd
     */
    function now_date() {
        var now = new Date(),
            year = now.getFullYear(),
            month = now.getMonth() + 1,
            day = now.getDate();
        var date = [year, month, day];
        var format = date.join("-");
        return format;
    }

    //比较日前大小  
    function compare_date(checkStartDate, checkEndDate) {
        var arys1 = new Array();
        var arys2 = new Array();
        if (checkStartDate != null && checkEndDate != null) {
            arys1 = checkStartDate.split('-');
            var sdate = new Date(arys1[0], parseInt(arys1[1] - 1), arys1[2]);
            arys2 = checkEndDate.split('-');
            var edate = new Date(arys2[0], parseInt(arys2[1] - 1), arys2[2]);
            if (sdate > edate) {
                log("date refuse");
                return false;
            } else {
                log("date ok");
                return true;
            }
        }
    }
}

/**
 * 活动-代养
 * @param  {obj} $scope [description]
 * @param  {obj} Page   [description]
 * @return {obj}        [description]
 */
function ctrl_activity_cultivation($scope, Page, Token, $http, $state) {
    Page.setTitle('代养服务');
    $scope.getValue = function(elem) {
        console.log(elem)
        cost_price(function(data) {
            log(data)
            if (data.result != -1) {
                cost.set(data.price)
            } else cost.set('0');
        });
    };

    $scope.goods = [{
        name: "生态鸡",
        id: "1",
        description: "三黄鸡由朱元璋赐名，肉质细嫩，营养丰富",
        num: 0
    }, {
        name: "生态鸭",
        id: "2",
        description: "将白日梦错认为现实，狂热且盲目。",
        num: 0
    }, {
        name: "生态鸡",
        id: "3",
        description: "或许真相存在与不起眼的事物中。",
        num: 0
    }];

    /**
     * hint init
     * @type {Object}
     */
    $scope.hintdate = {
        content: '土地只能种下这么多的蔬菜了~',
        show: false
    };

    function hint_delay(content) {
        $scope.hintdate = {
            content: content,
            show: false
        };
        hint.delay(4000);
    }

    // 弹出层管理器
    var dialog = new Popup($scope, 'toastdata');
    var hint = new Popup($scope, 'hintdate');

    var xhr = new Jsonp($http);
    var cost = new Cost($scope, 'costdata');

    //获取商品信息
    get_cultivation_msg();
    Token.get(function(data) {
        get_cultivation_msg();
    })

    function get_cultivation_msg() {
        xhr.get(_SERVER_ + "/fowlGet?access_token=" + _ACCESSTOKEN_ + "&callback=JSON_CALLBACK", function(data) {
            log(data);
            $scope.animal = data;
        });
    }

    //价格计数器
    $scope.costdata = {
        cost: 0,
        btn_centent: '下一步',
        next: function() {
            if (is_num_empty($scope.goods)) {
                sessionStorage.setItem("cultivation_chick", $scope.goods[0].num);
                sessionStorage.setItem("cultivation_duckling", $scope.goods[1].num);
                sessionStorage.setItem("cultivation_gosling", $scope.goods[2].num);
                $state.go('activity.pay', {
                    couponGroupIds: 'undefined',
                    cost: cost.get(),
                    planTime: "undefined",
                    url: "http://wx.imlenong.com/pay/pay_cultivation.html"
                });
            }else hint_delay("请选择家禽进行代养~");
        }
    };

    function cost_price(callback) {
        var chick = $scope.goods[0].num;
        var duckling = $scope.goods[1].num;
        var gosling = $scope.goods[2].num;
        var get_price_url = _SERVER_ + "/fowlAddPrice?access_token=" + _ACCESSTOKEN_ + "&chick=" + chick + "&duckling=" + duckling + "&gosling=" + gosling + "&callback=JSON_CALLBACK";
        xhr.get(get_price_url, function(data) {
            callback(data);
        });
    }

    function is_num_empty(goods) {
        for (var i = goods.length - 1; i >= 0; i--) {
            if (goods[i].num) {
                return true;
            }
        }
        return false;
    }
}

/**
 * 活动-游玩
 * @param  {obj} $scope [description]
 * @param  {obj} $http  [description]
 * @param  {obj} $state [description]
 * @param  {obj} Page   [description]
 * @param  {obj} Token  [description]
 * @return {}        [description]
 */
function ctrl_activity_play($scope, $http, $state, Page, Token) {
    Page.setTitle('农场游玩');
    //日历控件
    $(function() {
        $('#dd').calendar({
            trigger: '#dt',
            zIndex: 999,
            format: 'yyyy-mm-dd',
            onSelected: function(view, date, data) {
                console.log('event: onSelected')
            },
            onClose: function(view, date, data) {
                console.log('event: onClose')
                console.log('view:' + view)
                console.log('date:' + date)
                console.log('data:' + (data || 'None'));
            }
        });
    });
    //价格计数器
    $scope.costdata = {
        cost: 0,
        btn_centent: '下一步',
        next: function() {
            var plan_time = $$('dt').value;
            if (plan_time) { //日期为空时
                log('date is selected');

                if (compare_date(now_date(), plan_time)) { //日期不正确时
                    var timestamp = Date.parse(plan_time);

                    if (!arr_empty(goods_id)) { //未选择项目时

                        log(goods_id)
                        if (is_store(goods_id)) {
                            $state.go('activity.pay', {
                                couponGroupIds: '[' + goods_id.join(',') + ']',
                                cost: cost.get(),
                                planTime: timestamp,
                                url: "http://wx.imlenong.com/pay/pay.html"
                            });
                        } else {
                            $scope.hintdate = {
                                content: '套餐余量不足~',
                                show: false
                            };
                            hint.delay(4000);
                        }

                    } else {
                        $scope.hintdate = {
                            content: '请选择游玩项目~',
                            show: false
                        };
                        hint.delay(4000);
                    }

                } else {
                    $scope.hintdate = {
                        content: '请选择正确的日期~',
                        show: false
                    };
                    hint.delay(4000);
                }

            } else {
                $scope.hintdate = {
                    content: '请选择日期~',
                    show: false
                };
                hint.delay(4000);
            }
        }
    };
    /**
     * 余量
     * @param  {id}  goods_id 商品id
     * @return {Boolean}          [description]
     */
    function is_store(goods_id) {
        var _STOCK_ = 0;
        for (var i = goods_id.length - 1; i >= 0; i--) {
            //遍历id
            var id = goods_id[i];
            log($scope.goods)
            for (var j = $scope.goods.length - 1; j >= 0; j--) {
                // log($scope.goods[j].id)
                //找出id对应的余量
                // log($scope.goods[j].stock)
                if ($scope.goods[j].id == id && $scope.goods[j].stock > _STOCK_) {
                    return true;
                } else return false;
            }
        }
    }

    //弹出层
    // $scope.hintdate = {
    //     content: '',
    //     show: false
    // };

    // 弹出层管理器
    var hint = new Popup($scope, 'hintdate');

    $scope.isGoPlant = true; //游玩项目选项卡
    $scope.isGoToPlant = false;
    $scope.isGoToDiet = false;
    $scope.isGoToBicycle = false;
    $scope.isGoToBBQ = false;
    /**
     * 将所选的选项卡id存为数组，商品选择时直接对该数组进行维护
     * @type {Array}
     */
    var goods_id = [];
    //项目选择切换
    $scope.paper_toggle = function(toggle, elem) {
        if ($scope[toggle]) {
            //取消选择
            $scope[toggle] = false;
            remove(goods_id, elem.id);
            log(goods_id);
            var now_price = cost.get() - elem.price;
            cost.set(now_price);
        } else {
            //选中时
            $scope[toggle] = true;
            goods_id.push(elem.id);
            log(goods_id.join(","));
            cost.set(elem.price);
        }
    };
    // $scope.goods = {};
    /**
     * 维护底部计数器
     * @type {Cost}
     */
    var cost = new Cost($scope, 'costdata');
    var goods = new Jsonp($http);

    //获取商品信息
    goods.get(__API__._GetGoods_ + _ACCESSTOKEN_ + "&callback=JSON_CALLBACK", function(data) {
        log(data);
        $scope.goods = data.couponGroups;
    });
    Token.get(function(data) {
        goods.get(__API__._GetGoods_ + _ACCESSTOKEN_ + "&callback=JSON_CALLBACK", function(data) {
            log(data);
            $scope.goods = data.couponGroups;
        });
    })

    /**
     * 获取最新日期
     * @return {date} yyyy-mm-dd
     */
    function now_date() {
        var now = new Date(),
            year = now.getFullYear(),
            month = now.getMonth() + 1,
            day = now.getDate();
        var date = [year, month, day];
        var format = date.join("-");
        return format;
    }

    //比较日前大小  
    function compare_date(checkStartDate, checkEndDate) {
        var arys1 = new Array();
        var arys2 = new Array();
        if (checkStartDate != null && checkEndDate != null) {
            arys1 = checkStartDate.split('-');
            var sdate = new Date(arys1[0], parseInt(arys1[1] - 1), arys1[2]);
            arys2 = checkEndDate.split('-');
            var edate = new Date(arys2[0], parseInt(arys2[1] - 1), arys2[2]);
            if (sdate > edate) {
                log("date refuse");
                return false;
            } else {
                log("date ok");
                return true;
            }
        }
    }
    /**
     * 判断空数组
     * @param  {arr} arr [description]
     * @return {bool}     [description]
     */
    function arr_empty(arr) {
        if (arr.length == 0)
            return true;
        else return false;
    }
    /**
     * 移除指定的数组项
     * @param  {arr} arr [description]
     * @param  {val} val [description]
     * @return {}     [description]
     */
    function remove(arr, val) {
        var index = arr.indexOf(val);
        if (index > -1) {
            arr.splice(index, 1);
        }
    }

}
/**
 * 活动-支付
 * @param  {obj} $scope       [description]
 * @param  {obj} $http        [description]
 * @param  {obj} $stateParams 传入 $stateParams.couponGroupIds,$stateParams.planTime
 * @param  {obj} $state       [description]
 * @return {}              [description]
 */
function ctrl_activity_pay($scope, $http, $stateParams, $state) {
    var goods_id = $stateParams.couponGroupIds;
    var plan_time = $stateParams.planTime;
    // log(sessionStorage.getItem("nick_name"))
    if (sessionStorage.getItem("nickName")) {
        $$('name').value = sessionStorage.getItem("nickName");
    }
    if (sessionStorage.getItem("mobile")) {
        $$('phone').value = sessionStorage.getItem("mobile");
    }
    if (sessionStorage.getItem("nick_name")) {
        $$('name').value = sessionStorage.getItem("nick_name");
    }
    if (sessionStorage.getItem("phone_num")) {
        $$('phone').value = sessionStorage.getItem("phone_num");
    }

    var xhr = new Jsonp($http);

    // $scope.go_pay = function() {};
    $scope.costdata = {
        cost: $stateParams.cost,
        btn_centent: '去付款',
        next: function() {}
    };
    //表单检验
    $$('user_msg').onsubmit = function(e) {
            e.preventDefault();
            if (document.forms['user_msg'].checkValidity()) {
                var phone_num = $$('phone').value;
                var nick_name = $$('name').value;
                // phone_num = "18680079677";

                sessionStorage.setItem("goods_id", goods_id);
                sessionStorage.setItem("phone_num", phone_num);
                sessionStorage.setItem("nick_name", nick_name);
                sessionStorage.setItem("plan_time", plan_time);

                // location.href = "http://wx.imlenong.com/pay/pay.html";
                location.href = $stateParams.url;

                // var get_indent_url = "http://106.75.137.2:8088/lenong/goodsOrderAdd?access_token=" + _ACCESSTOKEN_ + "&couponGroupIds=" + encodeURIComponent(goods_id) + "&mobile=" + phone_num + "&nickName" + nick_name + "&planTime" + plan_time + "&callback=JSON_CALLBACK";
                // xhr.get(get_indent_url, function(data) {
                //     log(data);
                //     var indent_id = data.order.id;
                //     var pay_url = "http://106.75.137.2:8088/lenong/chargeCreate?access_token=" + _ACCESSTOKEN_ + "&orderId=" + indent_id + "&callback=JSON_CALLBACK";
                //     pay(pay_url);
                // })
            }
        }
        // function pay(pay_url) {
        //     xhr.get(pay_url, function(data) {
        //         log(data);
        //         var charge = data.pingxxCharge;
        //         pingpp.createPayment(charge, function(result, err) {
        //             console.log(result);
        //             console.log(err.msg);
        //             console.log(err.extra);
        //             if (result == "success") {
        //                 alert('pay ok');
        //                 $state.go('person.indent.fin');
        //                 // 只有微信公众账号 wx_pub 支付成功的结果会在这里返回，其他的支付结果都会跳转到 extra 中对应的 URL。
        //             } else if (result == "fail") {
        //                 alert('pay fail');
        //                 // charge 不正确或者微信公众账号支付失败时会在此处返回
        //             } else if (result == "cancel") {
        //                 alert('pay cancel');
        //                 // 微信公众账号支付取消支付
        //             }
        //         });
        //     })
        // }
}
