/**
 * init
 * @authors flyteng (695508580@qq.com)
 * @date    2016-08-20 09:17:53
 * @version 1.0.0
 */
var __API__ = {
    _GetAtcScience_: 'http://wx.imlenong.com/weChat/index.html#/',
    _GetPediaFAll_: 'http://106.75.137.2:8888/api/admin/pedia', //获取所有标记
    _GetAtc_: 'http://106.75.137.2:8888/api/admin/pedia/', //获取文章
    //play
    _GetGoods_: 'http://mobile-api.imlenong.com:8088/lenong/goodsGet?access_token=',
    end: ''
};

var _ACCESSTOKEN_ = sessionStorage.getItem("accessToken");
var _SERVER_ = "http://mobile-api.imlenong.com:8088/lenong";

/**
 * wechat_activity Module
 *
 * Description
 */
var wechat = angular.module('wechat', ['ui.router']);

//为了解决直接跳到某个子状态时父状态的数据不能同步到子状态
//
// wechat.all('*', function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
//     res.header("X-Powered-By", ' 3.2.1');
//     res.header("Content-Type", "application/json;charset=utf-8");
//     next();
// });

//弹出层
wechat.directive('alertMsg', function() {
    // Runs during compile
    return {
        scope: {
            toastdata: "=",
            close: '&'
        }, // {} = isolate, true = child, false/undefin ed = no change
        controller: function($scope, $element, $attrs, $transclude) {
            $scope.toast_close = function() {
                $scope.toastdata.show = false;
            }
            $scope.toast_cancel = function() {

            }
            $scope.toast_ok = function() {
                $scope.toastdata.show = false;
            }
        },
        restrict: 'AE', // E d= Element, A = Attribute, C = Class, M = Comment
        templateUrl: './tem/public/alertMsg.html',
        replace: true,
    };
});
//弹出层
wechat.directive('hintMsg', function() {
    // Runs during compile
    return {
        scope: {
            hintdate: "="
        }, // {} = isolate, true = child, false/undefin ed = no change
        controller: function($scope, $element, $attrs, $transclude) {
            $scope.hint_close = function() {
                $scope.hintdate.show = false;
            }
        },
        restrict: 'AE', // E d= Element, A = Attribute, C = Class, M = Comment
        templateUrl: './tem/public/hintMsg.html',
        replace: true,
    };
});
//价格计数器
wechat.directive('costBtm', function() {
    // Runs during compile
    return {
        scope: {
            costdata: "=",
            next: '&'
        }, // {} = isolate, true = child, false/undefin ed = no change
        controller: function($scope, $element, $attrs, $transclude) {

        },
        restrict: 'AE', // E d= Element, A = Attribute, C = Class, M = Comment
        templateUrl: './tem/public/costBtm.html',
        replace: true,
    };
});
wechat.directive('btmPay', function() {
    // Runs during compile
    return {
        scope: {
            paydata: "=",
        }, // {} = isolate, true = child, false/undefin ed = no change
        controller: function($scope, $element, $attrs, $transclude) {

        },
        restrict: 'AE', // E d= Element, A = Attribute, C = Class, M = Comment
        templateUrl: './tem/public/btmPay.html',
        replace: true,
    };
});
//ng-repeat end
//ng-repeat结束且dom渲染完成时触发
wechat.directive('repeatFinish', function($timeout) {
    return {
        link: function(scope, element, attr) {
            // console.log(scope.$index)
            if (scope.$last == true) {
                $timeout(function() {
                    // console.log('ng-repeat执行完毕')
                    scope.$eval(attr.repeatFinish)
                }, 0, false);
            }
        }
    }
});
//控制弹出层
// wechat.factory('Toast', function() {
//     // var show = false;
//     return {
//         open: function(scope) {
//             scope.toastdata.show = true;
//         },
//         close: function(scope) {
//             scope.toastdata.show = false;
//         },
//         delay: function(scope, time, callback) {
//             scope.toastdata.show = true;
//             setTimeout(function() {
//                 console.log('timeout')
//                 scope.toastdata.show = false;
//                 scope.$digest();
//                 callback();
//             }, time);
//         }
//     }
// });

//页首title
wechat.factory('Page', function() {
    var title = 'default';
    return {
        title: function() {
            return title;
        },
        setTitle: function(newTitle) {
            title = newTitle;
            log(title);
        }
    };
});
//权限获取
wechat.factory('Token', function($http) {
    return {
        get: function(callback) {
            var xhr = new Jsonp($http);
            var code = getQueryString(window.location.href, "code");
            var get_token_url = _SERVER_ + "/wxUserLogin?code=" + code + "&callback=JSON_CALLBACK";
            xhr.get(get_token_url, function(data) {
                log(data)
                    // $rootScope.user_msg = data.user;
                    // sessionStorage.clear();
                sessionStorage.setItem("accessToken", data.user.accessToken);
                sessionStorage.setItem("headImg", data.user.headImg);
                sessionStorage.setItem("nickName", data.user.nickName);
                sessionStorage.setItem("mobile", data.user.mobile);
                _ACCESSTOKEN_ = sessionStorage.getItem("accessToken");
                console.log(_ACCESSTOKEN_);
                callback(data);
            })
        }
    };
});

// Define a factory
// wechat.factory('profilePromise', ['$q', 'AccountService', function($q, AccountService) {
//     var deferred = $q.defer();
//     AccountService.getProfile().then(function(res) {
//         deferred.resolve(res);
//     }, function(res) {
//         deferred.resolve(res);
//     });
//     return deferred.promise;
// }]);


// use a factory
// parent controller user
// $scope.getPersonalInfo = function() {
//           profilePromise.then(function(res) {
//               var profile = res.content;
//           ........

// // define a scope used on child
// $scope.profilePromise = profilePromise;

//配置状态
wechat.config(router);

wechat.controller('dispatcher', function($scope, $http, Page, $rootScope, Token) {
    $scope.Page = Page;
    Page.setTitle('御农庄园');
});

function router($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.when('', '/bootstrap').otherwise('/bootstrap');

    $stateProvider
        .state('bootstrap', {
            url: '/bootstrap',
            templateUrl: './tem/bootstrap.html',
            controller: ctrl_bootstrap
        })
        //活动界面
        .state('activity', {
            abstract: true,
            url: '/activity',
            templateUrl: './tem/activity.html'
        })
        //农场主
        .state('activity.farmer', {
            url: '/farmer',
            templateUrl: './tem/activity/farmer.html',
            controller: ctrl_activity_farmer
        })
        .state('activity.seed', {
            url: '/seed/:farmId/:buyCrop',
            templateUrl: './tem/activity/seed.html',
            controller: ctrl_activity_seed
        })
        //养殖
        .state('activity.cultivation', {
            url: '/cultivation',
            templateUrl: './tem/activity/cultivation.html',
            controller: ctrl_activity_cultivation
        })
        //游玩
        .state('activity.play', {
            url: '/play',
            templateUrl: './tem/activity/play.html',
            controller: ctrl_activity_play
        })
        .state('activity.pay', {
            url: '/pay/:couponGroupIds/:cost/:planTime/:url',
            templateUrl: './tem/activity/pay.html',
            controller: ctrl_activity_pay
        })
        //科普
        .state('science', {
            abstract: true,
            url: '/science',
            templateUrl: './tem/science.html',
        })
        //科普地图
        .state('science.map', {
            url: '/map',
            templateUrl: './tem/science/map.html',
            controller: ctrl_science_map
        })
        //科普介绍
        .state('science.recomm', {
            url: '/recomm/:pedia_id',
            templateUrl: './tem/science/recomm.html',
            controller: ctrl_science_recomm
        })
        //个人界面
        .state('person', {
            url: '/person',
            templateUrl: './tem/person.html',
            controller: ctrl_person
        })
        //查看农场
        .state('person.farm', {
            url: '/farm',
            templateUrl: './tem/person/farm.html',
            controller: ctrl_person_farmer
        })
        //查看养殖
        .state('person.animal', {
            url: '/animal',
            templateUrl: './tem/person/animal.html',
            controller: ctrl_person_animal
        })
        //查看个人订单
        .state('person.indent', {
            abstract: true,
            url: '/indent',
            templateUrl: './tem/person/indent.html',
        })
        //订单已完成
        .state('person.indent.fin', {
            url: '/fin',
            templateUrl: './tem/public/indent_fin.html',
            controller: ctrl_person_indent_fin
        })
        //订单未完成
        .state('person.indent.unfin', {
            url: '/unfin',
            templateUrl: './tem/public/indent_unfin.html',
            controller: ctrl_person_indent_unfin
        })
}
/**
 * 引导页
 * @return {} [description]
 */
function ctrl_bootstrap() {}
