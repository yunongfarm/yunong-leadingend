/**
 * 
 * @authors Your Name (you@example.org)
 * @date    2016-07-14 14:55:27
 * @version $Id$
 */
var plant_msg_test = [{
    'Cstatus': 'w',
    'Fstatus': true,
    'Amun': '10',
    'time': '2016-7-1 16:53'
}, {
    'Cstatus': 'm',
    'Fstatus': true,
    'Amun': '12',
    'time': '2016-7-1 16:53'
}, {
    'Cstatus': 'g',
    'Fstatus': false,
    'Amun': '15',
    'time': '2016-7-1 16:53'
}, {
    'Cstatus': 'i',
    'Fstatus': false,
    'Amun': '17',
    'time': '2016-7-1 16:53'
}, {
    'Cstatus': 'w',
    'Fstatus': true,
    'Amun': '11',
    'time': '2016-7-1 16:53'
}, {
    'Cstatus': 'm',
    'Fstatus': true,
    'Amun': '13',
    'time': '2016-7-1 16:53'
}, {
    'Cstatus': 'g',
    'Fstatus': false,
    'Amun': '17',
    'time': '2016-7-1 16:53'
}];
/**
 * -------------------------------------------------------------------------农夫模块
 * @param  {[type]} $stateParams [description]
 * @return {[type]}              [description]
 */
/**
 * 种植信息
 * @param  {[type]} $stateParams [description]
 * @param  {[type]} $scope       [description]
 * @param  {[type]} $state       [description]
 * @return {[type]}              [description]
 */
function ctrl_farmer__plant__plant_msg($stateParams, $scope, $state, $rootScope) {
    $scope.dataset = plant_msg_test;
    $scope.order = 'Cstatus';
    $scope.query = '';

    $rootScope.msg_callback = function() {

        var goback = function() {
            console.log('msg back')
            history.back();
        };
        return { goback: goback }
    };
    /**
     * 控制msg
     * @param  {[type]} Fstatus [description]
     * @param  {[type]} elem    [description]
     * @return {[type]}         [description]
     */
    $scope.finish_msg = function(Fstatus, elem) {
        if (!Fstatus) {
            console.log(elem)
                /**
                 * 消息状态
                 */
            $state.go('farmer__plant.farmer__plant__plant_msg.msg_nal');
        }
    };
    /**
     * 返回状态对应的类
     * @param  {[type]} status [description]
     * @return {[type]}        [description]
     */
    $scope.select_class = function(status) {
        switch (status) {
            case 'w':
                return 'farmer_areamsg_w';
                break;
            case 'm':
                return 'farmer_areamsg_m';
                break;
            case 'g':
                return 'farmer_areamsg_g';
                break;
            case 'i':
                return 'farmer_areamsg_i';
                break;
        }
    };
}

// function ctrl_farmer__plant__plant_msg_msg_nal($stateParams, $scope) {
//     var plant_msg_alert_test = {
//         title: '确认信息',
//         content: '确认完成当前操作',
//         concel: {
//             content: '取消',
//             display: true
//         },
//         ok: {
//             content: '确认',
//             display: true
//         }
//     }
//     $scope.data = plant_msg_alert_test;
//     //console.log($scope.data.title)
//     $scope.concel = function() {
//         history.back();
//     };
//     $scope.ok = function() {};
// }
var farmer__plant_test = [{
    Pcode: '1',
    categroy: [{
        name: '蘑菇',
        statu: [{
            statu_name: 'mature',
            mature: false
        }, {
            statu_name: 'withered',
            withered: false
        }]
    }, {
        name: '草莓',
        statu: [{
            statu_name: 'mature',
            mature: false
        }, {
            statu_name: 'withered',
            withered: false
        }]
    }, {
        name: '地瓜',
        statu: [{
            statu_name: 'mature',
            mature: false
        }, {
            statu_name: 'withered',
            withered: false
        }]
    }],
    ctrl_time: {
        a_w: '2016-6-23 6:20',
        a_m: '2016-6-21 6:20',
        a_g: '2016-6-22 6:20',
        a_i: '2016-6-22 6:20'
    },
    plant_time: '2016-6-17',
    mature_time: '2016-7-17'
}, {
    Pcode: '2',
    categroy: [{
        name: '蘑菇',
        statu: [{
            statu_name: 'mature',
            mature: false
        }, {
            statu_name: 'withered',
            withered: false
        }, {
            statu_name: 'collected',
            withered: false
        }]
    }, {
        name: '草莓',
        statu: [{
            statu_name: 'mature',
            mature: false
        }, {
            statu_name: 'withered',
            withered: false
        }]
    }, {
        name: '地瓜',
        statu: [{
            statu_name: 'mature',
            mature: false
        }, {
            statu_name: 'withered',
            withered: false
        }]
    }],
    ctrl_time: {
        a_w: '2016-6-23 6:20',
        a_m: '2016-6-21 6:20',
        a_g: '2016-6-22 6:20',
        a_i: '2016-6-22 6:20'
    },
    plant_time: '2016-6-17',
    mature_time: '2016-7-17'
}];

function ctrl_farmer__plant__plant_list($stateParams, $scope, $rootScope) {
    $rootScope.plant_list_dataset = farmer__plant_test;
}

function ctrl_farmer__plant__plant_list__detail($stateParams, $scope, $rootScope) {
    console.log(farmer__plant_test[$stateParams.pcode - 1].categroy)
    $scope.plant_list_detail_dataset = farmer__plant_test[$stateParams.pcode - 1].categroy;
    $scope.back = state_back;
}

function state_back() {
    history.back();
}
