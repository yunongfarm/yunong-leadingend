/**
 * 科普地图
 * @param  {obj} $stateParams [description]
 * @param  {obj} $state       [description]
 * @param  {obj} $scope       [description]
 * @param  {obj} $http        [description]
 * @param  {obj} Page         factory
 * @return {obj}              [description]
 */
function ctrl_science_map($stateParams, $state, $scope, $http, Page) {
    Page.setTitle('科普地图');
    /**
     * 构建地图
     * @type {AMap}
     */
    var mapObj = new AMap.Map('map_container', {
        zoom: 3, //缩放级别
        center: [113.544184, 22.214936], //中心点
        isHotspot: true, //地图热点           
        keyboardEnable: true, //键盘控制
        mapStyle: 'fresh', //normal（默认样式）、dark（深色样式）、light（浅色样式）、fresh(osm样式)
        // touchZoom: false,
        zooms: [3, 6]
    });
    /**
     * 工具条&比例尺
     * @param  {AMap}   ) {                           var toolBar [description]
     * @return {}   [description]
     */
    AMap.plugin(['AMap.ToolBar', 'AMap.Scale'], function() {
        //TODO  创建控件并添加
        var toolBar = new AMap.ToolBar();
        var scale = new AMap.Scale();
        mapObj.addControl(toolBar);
        mapObj.addControl(scale);
    });
    /**
     * 创建提示窗体
     * @type {AMap}
     */
    var infowindow = new AMap.InfoWindow({
        content: '<p>ok</p>',
        offset: new AMap.Pixel(0, -30),
        size: new AMap.Size(230, 0)
    });
    //init
    var md = new EditForm();//专门的类来维护这些字段
    md.init({
        pedia_id: '',
        title: '',
        city: '',
        province: '',
        pushpin: '',
        category: '',
        context: '',
        location: '',
        markdown: '',
        coverurl: ''
    });
    var imarker = new Imarker(mapObj, $http, md, infowindow);
    imarker.get_all(function() {
        imarker.init_marker_lister();
    });
    /**
     * 自适应调整缩放比例
     */
    mapObj.setFitView();
}

/**
 * 监听标记点击事件
 * @param  {} var i             [description]
 * @return {}     [description]
 */
// for (var i = markers.length - 1; i >= 0; i--) {
//     AMap.event.addListener(markers[i], 'click', function(ev) {
//         console.log(this)
//         infowindow.setContent(ev.target.content);
//         infowindow.open(mapObj, this.getPosition());
//     });
// }

/**
 * 科普文章介绍
 * @param  {obj} $stateParams [description]
 * @param  {obj} $state       [description]
 * @param  {obj} $http        [description]
 * @param  {obj} $scope       [description]
 * @return {obj}              [description]
 */
function ctrl_science_recomm($stateParams, $state, $http, $scope) {
    console.log($stateParams);
    var pedia_id = $stateParams.pedia_id;
    var md = new EditForm();
    md.init({
        pedia_id: '',
        title: '',
        city: '',
        province: '',
        pushpin: '',
        category: '',
        context: '',
        location: '',
        markdown: '',
        coverurl: ''
    });
    var mark_node = $$('waiting');
    var loading = new Loading(mark_node);
    var art = new Article($http, $stateParams, $state, loading, md);
    //拉取文章
    art.get(pedia_id);
}
