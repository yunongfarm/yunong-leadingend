function ctrl_farm($scope, $http, Page, $stateParams) {
    var xhr = new Jsonp($http);

    xhr.get(_SERVER_ + "/framGetAll?access_token=" + _ACCESSTOKEN_ + "&callback=JSON_CALLBACK", function(data) {
        log(data);
        $scope.farms = data.allFarm;
    });

    $scope.isJudge = function(bool) {
        if (bool) return "已购买";
        else return "未购买";
    }

}

function ctrl_farm_detail($scope, $http, Page, $stateParams) {
    $scope.status = [
        { status: 0, name: "未播种" },
        { status: 1, name: "成长期" },
        { status: 2, name: "成熟期" },
        { status: 3, name: "已采摘" },
        { status: -1, name: "已枯萎" }
    ];
    $scope.update_status = function(status, id) {
        log(status)
        log(id)
        var update_status_url = _SERVER_ + "/userCropUpdate?access_token=" + _ACCESSTOKEN_ + "&myCropId=" + id + "&cropStatus=" + status + "&callback=JSON_CALLBACK";
        xhr.get(update_status_url, function(data) {
            log(data);
            xhr.get(_SERVER_ + "/framGetSingle?access_token=" + _ACCESSTOKEN_ + "&farmId=" + farmId + "&callback=JSON_CALLBACK", function(data) {
                log(data);
                $scope.farm = data.farm;
            });
        });
    };
    $scope.isJudge = function(bool) {
        if (bool) return "是";
        else return "否";
    }
    $scope.crop_status = function(status) {
        // log(status,'status')
        switch (status) {
            case 0:
                return "未播种";
                break;
            case 1:
                return "成长中";
                break;
            case 2:
                return "已成熟";
                break;
            case 3:
                return "已采摘";
                break;
            case -1:
                return "已枯萎";
                break;
        }
    }
    var xhr = new Jsonp($http);
    var farmId = $stateParams.farmId;
    xhr.get(_SERVER_ + "/framGetSingle?access_token=" + _ACCESSTOKEN_ + "&farmId=" + farmId + "&callback=JSON_CALLBACK", function(data) {
        log(data);
        $scope.farm = data.farm;
    });
}
