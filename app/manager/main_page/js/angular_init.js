/**
 * angular init
 * __authors flyteng (you__example.org)
 * __date    2016-06-26 14:59:00
 * __version $Id$
 */
/**
 * mager Module
 *
 * Description
 */
var __API__ = {
    _Root_: "http://106.75.137.2:8888/api",
    //pedia
    _AddPediaF_: 'http://106.75.137.2:8888/api/admin/pedia', //添加标记
    _GetPediaFAll_: 'http://106.75.137.2:8888/api/admin/pedia', //获取所有标记
    _GetAtc_: 'http://106.75.137.2:8888/api/admin/pedia/', //获取文章
    _GetAtcAll_: 'http://106.75.137.2:8888/api/admin/pedia/all',
    _DelAtc_: 'http://106.75.137.2:8888/api/admin/pedia/', //删除文章
    _GetIntro_:'http://106.75.137.2:8888/api/admin/intro/',//园区介绍
    _UpIntro_:'http://106.75.137.2:8888/api/admin/intro',
    //farm_mager
    _AddPlant_: 'http://106.75.137.2:8888/api/admin/lands/',//post
    _DelPlant_: 'http://106.75.137.2:8888/api/admin/lands/',//delete
    _UpPlant_: 'http://106.75.137.2:8888/api/admin/lands/',//put
    _GetPlant_: '',
    _GetPlantAll_: 'http://106.75.137.2:8888/api/admin/lands/',//get
    //pck
    _GetPck_: '',
    _AddPck_: '',
    _DelPck_: '',
    _UpPck_: ''
};

var mager_mod = angular.module('mager', ['ui.router']);
mager_mod.controller('mager_msg', function($scope) {
    $scope.mager = {
        name: '阿斯顿',
        logout: function() {
            console.log('youLogout')
        }
    }

});
/**
 * 状态路由配置
 */
mager_mod.config(mager_route_cof);

/**
 * 板块路由配置以及状态的建立
 * @param  {[type]} $stateProvider     [description]
 * @param  {[type]} $urlRouterProvider [description]
 * @return {[type]}                    [description]
 */
function mager_route_cof($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.when('', '/main_select').otherwise('/main_select');

    $stateProvider
        .state('main_select', {
            url: '/main_select',
            templateUrl: './tem/main_select.html',
            controller: ctrl_main_select
        })
        /**
         * -------------------------------------------------------农夫板块
         * @type {Boolean}
         */
        .state('farmer__plant', {
            /**
             * 默认不触发，没有ctrl存在的必要
             * @type {Boolean}
             */
            abstract: true,
            url: '/farmer__plant',
            templateUrl: './tem/farmer__plant.html',
        })
        /**
         * 种植信息
         * @type {String}
         */
        .state('farmer__plant.farmer__plant__plant_msg', {
            url: '/farmer__plant/farmer__plant__plant_msg',
            templateUrl: './tem/farmer__plant__plant_msg.html',
            controller: ctrl_farmer__plant__plant_msg
        })
        /**
         * 子状态（信息）
         * @type {String}
         */
        .state('farmer__plant.farmer__plant__plant_msg.msg_nal', {
            url: '/farmer__plant/farmer__plant__plant_msg/msg_nal',
            templateUrl: './tem/msg_nal.html',
            controller: ctrl_msg_nal
        })
        /**
         * 种植列表（土地）
         * @type {String}
         */
        .state('farmer__plant.farmer__plant__plant_list', {
            url: '/farmer__plant/farmer__plant__plant_list',
            templateUrl: './tem/farmer__plant__plant_list.html',
            controller: ctrl_farmer__plant__plant_list
        })
        /**
         * 种植列表土地详情
         * @type {String}
         */
        .state('farmer__plant.farmer__plant__plant_list__detail', {
            url: '/farmer__plant/:pcode',
            templateUrl: './tem/farmer__plant__plant_list__detail.html',
            controller: ctrl_farmer__plant__plant_list__detail
        })
        /**
         * --------------------------------------------------------农场管理者板块
         * @type {Boolean}
         */
        .state('farm_mager__pub_area', {
            url: '/farm_mager__pub_area',
            templateUrl: './tem/farm_mager__pub_area.html',
            controller: ctrl_farm_mager__pub_area
        })
        .state('farm_mager__pub_server_pck', {
            url: '/farm_mager__pub_server_pck',
            templateUrl: './tem/farm_mager__pub_server_pck.html',
            controller: ctrl_farm_mager__pub_server_pck
        })
        /**
         * 子状态（信息）
         * @type {String}
         */
        .state('farm_mager__pub_server_pck.msg_nal', {
            url: '/farm_mager__pub_server_pck/msg_nal',
            templateUrl: './tem/msg_nal.html',
            controller: ctrl_msg_nal
        })
        //发布种子子模块
        .state('farm_mager__pub_seed_case', {
            abstract: true,
            url: '/farm_mager__pub_seed_case',
            templateUrl: './tem/farm_mager__pub_seed_case.html',
        })
        .state('farm_mager__pub_seed_case.farm_mager__pub_seed_case__seed_list', {
            url: '/farm_mager__pub_seed_case/farm_mager__pub_seed_case__seed_list',
            templateUrl: './tem/farm_mager__pub_seed_case__seed_list.html',
            controller: ctrl_farm_mager__pub_seed_case__seed_list
        })
        .state('farm_mager__pub_seed_case.farm_mager__pub_seed_case__seed_case', {
            url: '/farm_mager__pub_seed_case/farm_mager__pub_seed_case__seed_case',
            templateUrl: './tem/farm_mager__pub_seed_case__seed_case.html',
            controller: ctrl_farm_mager__pub_seed_case__seed_case
        })
        /**
         * ---------------------------------------------------------APP管理者板块
         * @type {Boolean}
         */
        .state('app_mager__gar_recom', {
            url: '/app_mager__gar_recom/:province/:city/:location/:pedia_id',
            templateUrl: './tem/app_mager__gar_recom.html',
            controller: ctrl_app_mager__gar_recom
        })
        .state('app_mager__plant_science', {
            url: '/app_mager__plant_science',
            templateUrl: './tem/app_mager__plant_science.html',
            controller: ctrl_app_mager__plant_science
        })
        .state('app_mager__intro', {
            url: '/app_mager__intro',
            templateUrl: './tem/app_mager__map_science.html',
            controller: ctrl_app_mager__intro
        })
}

//-----------------------------------------------------------------------public_controller
/**
 * 普适消息模板
 * @param  {[type]} $stateParams [description]
 * @param  {[type]} $scope       [description]
 * @param  {[type]} $rootScope   [description]
 * @return {[type]}              [description]
 */
function ctrl_msg_nal($stateParams, $scope, $rootScope) {
    var msg_alert_test = {
        title: '确认信息',
        content: '确认完成当前操作',
        concel: {
            content: '取消',
            display: true
        },
        ok: {
            content: '确认',
            display: true
        }
    }
    $scope.data = msg_alert_test;
    //console.log($scope.data.title)
    $scope.concel = function() {
        history.back();
    };
    $scope.ok = function() {
        //console.log($rootScope.msg_callback)
        $rootScope.msg_callback().goback();
    };
}

/**
 * -----------------------------------------------------------------------------------------主面板
 * @param  {[type]} $stateParams [description]
 * @return {[type]}              [description]
 */
function ctrl_main_select($stateParams) {}

function getImgUrl(file) {
    var url = null;
    if (window.createObjectURL != undefined) { // basic
        url = window.createObjectURL(file);
    } else if (window.URL != undefined) { // mozilla(firefox)
        url = window.URL.createObjectURL(file);
    } else if (window.webkitURL != undefined) { // webkit or chrome
        url = window.webkitURL.createObjectURL(file);
    }
    return url;
}
