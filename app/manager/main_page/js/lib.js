/**
 * lib
 * @authors Your Name (you@example.org)
 * @date    2016-07-22 17:51:13
 * @version $Id$
 */

function mconsole(atl, value) {
    console.log('[' + atl + ']')
    console.log(value)
}

function log(content) {
    var args = Array.prototype.slice.call(arguments);
    args.unshift('[app]:');
    console.log.apply(console, args);
}

var $$ = function(id) {
    return document.getElementById(id);
};

/**
 * 随机数生成
 * @param  {[type]} n [description]
 * @return {[type]}   [description]
 */
function generateMixed(n) {
    var chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    var res = "";
    for (var i = 0; i < n; i++) {
        var id = Math.ceil(Math.random() * 35);
        res += chars[id];
    }
    return res;
}
/**
 * XHR
 * @return {[type]} [description]
 */
function createXHR() {
    if (window.XMLHttpRequest) {
        // 新浏览器
        return new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        // IE5,IE6
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
}

function xhrthen(xhr, callback) {
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            if (xhr.status >= 200 && xhr.status < 300 || xhr.status == 304) {
                console.log(xhr.responseText)
                callback();
            } else {
                console.log(xhr.status)
                console.log(xhr.responseText)
                callback();
            }
        }
    };
};
/**
 * 专门维护一个预设数据对象
 */
function EditForm() {
    this.init = function(oj) {
        if (oj === Object(oj)) {
            this.form = oj;
        } else false;
    }
    this.get = function(key) {
        if (this.form[key] != undefined) {
            return this.form[key];
        } else {
            return false;
        }
    };
    this.set = function(key, value) {
        if (this.form[key] != undefined) {
            this.form[key] = value;
            return this.form[key];
        } else {
            return false;
        }
    };
    this.set_form = function(oj) {
        for (var key in oj) {
            // console.log(this.form[key])
            if (this.form[key] != undefined) {
                this.form[key] = oj[key];
            }
        }
        return this.form;
    };
    this.get_form = function() {
        return this.form;
    };
    this.render_input = function(id, content) {
        var input = document.getElementById(id);
        if (content) {
            input.value = content;
        }
    };
    this.render_elem = function(id, content) {
        var elem = document.getElementById(id);
        if (content) {
            elem.innerHTML = content;
        }
    };
    this.render_img = function(id, img_url) {
        if (img_url) {
            var container = document.getElementById(id);
            var img = document.createElement('img');
            img.src = img_url;
            if (!container.hasChildNodes()) {
                container.appendChild(img);
            } else {
                container.replaceChild(img, container.firstChild);
            }
        }
    };
}
/**
 * 等待加载时的过度效果
 * @param {[type]} node [description]
 */
function Loading(node) {
    this.toggle = function() {
        if (node.classList.contains('d-n')) {
            node.classList.remove('d-n');
            node.classList.add('d-b');
        } else {
            node.classList.remove('d-b');
            node.classList.add('d-n');
        }
    };
    this.show = function() {
        if (node.classList.contains('d-n')) {
            node.classList.remove('d-n');
            node.classList.add('d-b');
        } else return false;
    };
    this.hide = function() {
        if (node.classList.contains('d-b')) {
            node.classList.remove('d-b');
            node.classList.add('d-n');
        } else return false;
    };
}
/**
 * 某一元素开启全屏模式
 * @param  {[type]} id [description]
 * @return {[type]}    [description]
 */
function full_screen_toggle(id) {
    var node = $$(id);
    if (node.classList.contains('full-screen')) {
        node.classList.remove('full-screen');
    } else {
        node.classList.add('full-screen');
    }
}
/**
 * 下滑时固定某一栏
 * @param  {[type]} id        [元素id]
 * @param  {[type]} CssConfig [自适应尺寸配置]
 * @return {[type]}           [description]
 */
function fix_top(id, CssConfig) {
    var elem = $$(id);
    if (document.body.scrollTop > 100) {
        if (!elem.classList.contains('fix-top')) {
            elem.classList.add('fix-top');
            elem.classList.add(CssConfig);
            console.log(document.body.scrollTop)
        }
    } else {
        if (elem.classList.contains('fix-top')) {
            elem.classList.remove('fix-top');
            elem.classList.remove(CssConfig);
            console.log(document.body.scrollTop)
        }
    }
}
