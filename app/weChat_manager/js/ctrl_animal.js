function ctrl_animal($scope, $http) {
    var xhr = new Jsonp($http)
    get_animals_msg();
    var _SORTTIME_ = '';

    $scope.more = function() {
        log('more')
        get_animals_msg_more(_SORTTIME_);
    }

    $scope.name_transfer = function(type) {
        // log(type)
        switch (type) {
            case 1:
                return "生态鸡";
            case 2:
                return "小鸭";
            case 3:
                return "小鹅";
        }
    };

    $scope.isJudge = function(bool) {
        if (bool==1) return "已购买";
        else return "未购买";
    }

    $scope.date_transfer = function(date) {
        // log(date)
        if (date) {
            return date.split(" ")[0];
        }
    }
    $scope.name_transfer = function(type) {
        // log(type)
        switch (type) {
            case 1:
                return "生态鸡";
            case 2:
                return "小鸭";
            case 3:
                return "小鹅";
        }
    };

    function get_animals_msg() {
        var url = _SERVER_ + "/fowlGetAll?access_token=" + _ACCESSTOKEN_ + "&callback=JSON_CALLBACK";
        xhr.get(url, function(data) {
            log('ok', 'T')
            log(data);
            $scope.animals = data.fowls;
            _SORTTIME_ = data.sortTime;
        });
    }

    function get_animals_msg_more(sortTime) {
        log(sortTime, 'ST');
        var url = _SERVER_ + "/fowlGetAll?access_token=" + _ACCESSTOKEN_ + "&sortTime=" + sortTime + "&callback=JSON_CALLBACK";
        xhr.get(url, function(data) {
            log(data);
            $scope.animals = $scope.animals.concat(data.fowls);
            _SORTTIME_ = data.sortTime;
        });
    }
}

function ctrl_animal_detail($scope, $http, $stateParams) {
    var animal_id = $stateParams.id;
    var xhr = new Jsonp($http);
    get_animal_msg(animal_id);

    function get_animal_msg(animal_id) {
        var url = _SERVER_ + "/adminFowlGet?access_token=" + _ACCESSTOKEN_ + "&fowlId=" + animal_id + "&callback=JSON_CALLBACK";
        log(url)
        xhr.get(url, function(data) {
            log(data);
            $scope.animal = data.fowl;
        });
    }


}

function ctrl_animal_config($scope, $http) {
    var xhr = new Jsonp($http)
    get_config();

    $scope.data_update = function() {
        var param = url_transfer($scope.allow_sell) + url_transfer($scope.capacity);
        var url = _SERVER_ + "/fowlSellCntUpdate?access_token=" + _ACCESSTOKEN_ + param + "&callback=JSON_CALLBACK";
        log(url)
        xhr.get(url, function(data) {
            log(data);
            get_config();
            if (data.result == 1) {
                alert("更新成功");
            }
        });
    }

    function url_transfer(obj) {
        var result = "";
        for (var key in obj) {
            result += '&' + key + '=' +
                obj[key];
            // log(obj[key])
        }
        return result;
    }

    function get_config() {
        var url = _SERVER_ + "/fowlSellCntGet?access_token=" + _ACCESSTOKEN_ + "&callback=JSON_CALLBACK";
        xhr.get(url, function(data) {
            log(data);
            $scope.config = data;
            $scope.allow_sell = {
                chickSellCnt: data.chickSellCnt,
                ducklingSellCnt: data.ducklingSellCnt,
                goslingSellCnt: data.goslingSellCnt
            };
            $scope.capacity = {
                chickMaxCnt: data.chickMaxCnt,
                ducklingMaxCnt: data.ducklingMaxCnt,
                goslingMaxCnt: data.goslingMaxCnt
            }
        });
    }
}
