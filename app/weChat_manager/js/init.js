/**
 * init
 * @authors Your Name (you@example.org)
 * @date    2016-08-20 09:17:53
 * @version $Id$
 */

var __API__ = {

};
var _ACCESSTOKEN_ = sessionStorage.getItem("accessToken");
var _SERVER_ = "http://mobile-api.imlenong.com:8088/lenong";
console.log(_ACCESSTOKEN_);
/**
 * wechat_activity Module
 *
 * Description
 */
var wechat = angular.module('wechat', ['ui.router']);

wechat.directive('alertMsg', function() {
    // Runs during compile
    return {
        scope: {
            toastdata: "=",
            close: '&'
        }, // {} = isolate, true = child, false/undefin ed = no change
        controller: function($scope, $element, $attrs, $transclude) {
            $scope.toast_close = function() {
                $scope.toastdata.show = false;
            }
            $scope.toast_cancel = function() {

            }
            $scope.toast_ok = function() {
                $scope.toastdata.show = false;
            }
        },
        restrict: 'AE', // E d= Element, A = Attribute, C = Class, M = Comment
        templateUrl: './tem/public/alertMsg.html',
        replace: true,
    };
});

wechat.directive('btmBtn', function() {
    // Runs during compile
    return {
        scope: {
            btmBtnData: "=",
            close: '&'
        }, // {} = isolate, true = child, false/undefin ed = no change
        controller: function($scope, $element, $attrs, $transclude) {
            $scope.back = function() {
                history.back();
            }
        },
        restrict: 'AE', // E d= Element, A = Attribute, C = Class, M = Comment
        templateUrl: './tem/public/btmbtn.html',
        replace: true,
    };
});
wechat.directive('hintMsg', function() {
    // Runs during compile
    return {
        scope: {
            hintdate: "="
        }, // {} = isolate, true = child, false/undefin ed = no change
        controller: function($scope, $element, $attrs, $transclude) {
            $scope.hint_close = function() {
                $scope.hintdate.show = false;
            }
        },
        restrict: 'AE', // E d= Element, A = Attribute, C = Class, M = Comment
        templateUrl: './tem/public/hintMsg.html',
        replace: true,
    };
});

wechat.factory('Page', function() {
    var title = 'default';
    return {
        title: function() {
            return title;
        },
        setTitle: function(newTitle) {
            title = newTitle;
            log(title);
        }
    };
});

wechat.config(router);

wechat.controller('dispatcher', function($scope, Page, $http, $rootScope) {
    //data bind
    $scope.Page = Page;
    Page.setTitle('菜单页');
    var xhr = new Jsonp($http);
    var code = getQueryString(window.location.href, "code");

    var get_token_url = _SERVER_ + "/wxUserLogin?code=" + code + "&callback=JSON_CALLBACK";
    xhr.get(get_token_url, function(data) {
        log(data)
        $rootScope.user_msg = data.user;
        sessionStorage.clear();
        sessionStorage.setItem("accessToken", data.user.accessToken);
        sessionStorage.setItem("headImg", data.user.headImg);
        sessionStorage.setItem("nickName", data.user.nickName);
        _ACCESSTOKEN_ = sessionStorage.getItem("accessToken");
        console.log(_ACCESSTOKEN_);
    })
});

function router($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.when('', '/bootstrap').otherwise('/bootstrap');

    $stateProvider
        .state('bootstrap', {
            url: '/bootstrap',
            templateUrl: './tem/bootstrap.html',
            controller: ctrl_bootstrap
        })
        //二维码
        .state('qrcode', {
            url: '/qrcode',
            templateUrl: './tem/qrcode.html',
            controller: ctrl_qrcode
        })
        //订单
        .state('indent', {
            url: '/indent',
            templateUrl: './tem/indent.html',
            controller: ctrl_indent
        })
        //订单详情
        .state('indent.normal', {
            url: '/normal',
            templateUrl: './tem/indent/normal.html',
            controller: ctrl_indent_normal
        })
        .state('indent.normal.detail', {
            url: '/detail/:indent_id',
            templateUrl: './tem/indent/normal/detail.html',
            controller: ctrl_indent_normal_detail
        })
        .state('indent.cancel', {
            url: '/cancel',
            templateUrl: './tem/indent/cancel.html',
            controller: ctrl_indent_cancel,
            cache: false
        })
        .state('indent.cancel.detail', {
            url: '/detail/:indent_id',
            templateUrl: './tem/indent/cancel/detail.html',
            controller: ctrl_indent_cancel_detail
        })
        //土地
        .state('plant', {
            url: '/plant',
            templateUrl: './tem/plant.html',
            controller: ctrl_plant
        })
        //土地详情
        .state('plant.detail', {
            url: '/detail',
            templateUrl: './tem/plant/detail.html',
            controller: ctrl_plant_detail
        })
        //代养
        .state('animal', {
            url: '/animal',
            templateUrl: './tem/animal.html',
            controller: ctrl_animal
        })
        //代养详情
        .state('animal.detail', {
            url: '/detail/:id',
            templateUrl: './tem/animal/detail.html',
            controller: ctrl_animal_detail
        })
        //代养农场配置
        .state('animal.config', {
            url: '/config',
            templateUrl: './tem/animal/config.html',
            controller: ctrl_animal_config
        })
        //农场
        .state('farm', {
            url: '/farm',
            templateUrl: './tem/farm.html',
            controller: ctrl_farm
        })
        //农场详情
        .state('farm.detail', {
            url: '/detail/:farmId',
            templateUrl: './tem/farm/detail.html',
            controller: ctrl_farm_detail
        })
        //bbq
        .state('bbq', {
            url: '/bbq',
            templateUrl: './tem/bbq.html',
            controller: ctrl_bbq
        })
        .state('bbq.edit', {
            url: '/edit',
            templateUrl: './tem/bbq/edit.html',
            controller: ctrl_bbq_edit
        })
        //蔬菜
        .state('greens', {
            url: '/greens',
            templateUrl: './tem/greens.html',
            controller: ctrl_greens
        })
        .state('greens.edit', {
            url: '/edit/:id',
            templateUrl: './tem/greens/edit.html',
            controller: ctrl_greens_edit
        })
        .state('greens.add', {
            url: '/add/:id',
            templateUrl: './tem/greens/add.html',
            controller: ctrl_greens_add
        })
}

//引导
function ctrl_bootstrap() {}
