function ctrl_indent() {}

function ctrl_indent_normal($scope, $http) {
    var xhr = new Jsonp($http)
    var _SORTTIME_ = '';
    var get_all_indent_url = _SERVER_ + "/orderAllGet?access_token=" + _ACCESSTOKEN_ + "&callback=JSON_CALLBACK";
    xhr.get(get_all_indent_url, function(data) {
        log(data);
        $scope.orders = data.orders;
        _SORTTIME_ = data.sortTime;
        log(_SORTTIME_)
    })
    console.log('ok')

    $scope.more = function() {
        if (_SORTTIME_) {
            var get_more_indent_url = _SERVER_ + "/orderAllGet?access_token=" + _ACCESSTOKEN_ + "&sortTime=" + _SORTTIME_ + "&callback=JSON_CALLBACK";
            xhr.get(get_more_indent_url, function(data) {
                log(data);
                $scope.orders.push.apply($scope, data.orders);
                _SORTTIME_ = data.sortTime;
                log(_SORTTIME_)
            })
        } else alert('没有更多了')
    }
}

function ctrl_indent_normal_detail($scope, $http, $stateParams) {
    var xhr = new Jsonp($http);
    $scope.change_used_status = function(list) {
        log(list)

        if (list.isUsed) {
            var isUsed = 0;
        } else var isUsed = 1;

        var goods_id = list.id;
        var change_used_status_url = _SERVER_ + "/myGoodsUpdate?access_token=" + _ACCESSTOKEN_ + "&myGoodsId=" + goods_id + "&isUsed=" + isUsed + "&callback=JSON_CALLBACK";

        xhr.get(change_used_status_url, function(data) {
            log(data);
            get_indent_msg();
        });

    }

    get_indent_msg();

    function get_indent_msg() {
        var indent_id = $stateParams.indent_id;
        var get_indent_url = _SERVER_ + "/orderGet?access_token=" + _ACCESSTOKEN_ + "&orderId=" + indent_id + "&callback=JSON_CALLBACK";
        xhr.get(get_indent_url, function(data) {
            log(data.order);
            $scope.indent = data.order;
            $scope.goods = data.order.myGoods;
        })
    }


}

function ctrl_indent_cancel($scope, $http) {
    log('detail')
    var xhr = new Jsonp($http);
    var _SORTTIME_ = '';
    var get_all_indent_url = _SERVER_ + "/orderCancelListGet?access_token=" + _ACCESSTOKEN_ + "&callback=JSON_CALLBACK";
    xhr.get(get_all_indent_url, function(data) {
        log(data);
        $scope.orders = data.orders;
        _SORTTIME_ = data.sortTime;
    })

    $scope.more = function() {
        if (_SORTTIME_) {
            var get_more_indent_url = _SERVER_ + "/orderCancelListGet?access_token=" + _ACCESSTOKEN_ + "&sortTime=" + _SORTTIME_ + "&callback=JSON_CALLBACK";
            xhr.get(get_more_indent_url, function(data) {
                log(data);
                $scope.orders.push.apply($scope, data.orders);
                _SORTTIME_ = data.sortTime;
                log(_SORTTIME_)
            })
        } else alert('没有更多了')
    }
}

function ctrl_indent_cancel_detail($scope, $http, $stateParams, $state) {
    log('cancel detail')
    var xhr = new Jsonp($http);
    get_indent_msg();

    $scope.indent_cancel = function(id) {
        // history.go(-1);
        // location.reload();
        // history.go(-1)
        var indent_cancel_url = _SERVER_ + "/updateCancelState?access_token=" + _ACCESSTOKEN_ + "&orderId=" + id + "&callback=JSON_CALLBACK";
        xhr.get(indent_cancel_url, function(data) {
            log(data)
            alert("退款成功");
            $state.go('indent.cancel', {}, { reload: true });
            // location.reload();
            // history.back(-1);
        })
        log('cancel')
    }

    function get_indent_msg() {
        var indent_id = $stateParams.indent_id;
        var get_indent_url = _SERVER_ + "/orderGet?access_token=" + _ACCESSTOKEN_ + "&orderId=" + indent_id + "&callback=JSON_CALLBACK";
        xhr.get(get_indent_url, function(data) {
            log(data.order);
            $scope.indent = data.order;
            $scope.goods = data.order.myGoods;
        })
    }
}
