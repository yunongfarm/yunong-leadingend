#leNong-leading end
##架构说明
- AngularJs整理交互逻辑
    - route-ui 整理组件逻辑 
- 原生js组件打造特殊效果 
- json格式传输数据

#app->wechat has updataed

#农夫模块
##种植>消息面板
###plant_msg
    {
        'Cstatus': 'm',
        'Fstatus': true,
        'Amun': '12',
        'time': '2016-7-1 16:53'
    }
Cstatus | Fstatus | Amun | time
----|------|----|----
用户发布的操作状态 | 后台操作行为的完成状态  | 土地编号 | 用户发布操作的时间
char w/s/g/i/p | bool  | char | date
分别为浇水，松土，除草，除虫，种植 |   |  | 

##/GET 用户发布的操作状态信息
##/POST 恢复到对应土地的对应正常状态(完成状态) | 或者可以恢复到未完成状态（防止误点）

##种植>土地面板
###plant_list
    {
    Pcode: '1',
    categroy: [{
        name: '蘑菇',
        statu: [{
            statu_name: 'mature',
            mature: false
        }, {
            statu_name: 'withered',
            withered: false
        }, {
            statu_name: 'collected',
            withered: false
        }]
    }, {
        name: '草莓',
        statu: [{
            statu_name: 'mature',
            mature: false
        }, {
            statu_name: 'withered',
            withered: false
        }, {
            statu_name: 'collected',
            withered: false
        }]
    }, {
        name: '地瓜',
        statu: [{
            statu_name: 'mature',
            mature: false
        }, {
            statu_name: 'withered',
            withered: false
        }, {
            statu_name: 'collected',
            withered: false
        }]
    }],
    ctrl_time: {
        a_w: '2016-6-23 6:20',
        a_m: '2016-6-21 6:20',
        a_g: '2016-6-22 6:20',
        a_i: '2016-6-22 6:20'
    },
    plant_time: '2016-6-17',
    mature_time: '2016-7-17'
    }
Pcode | categroy | ctrl_time | plant_time | mature_time
----|------|----|---|---
土地编号 | 作物  | 上次操作时间 | 种植时间 | 成熟时间
  |  生长状态 mature：成熟状态 |   |  | a_w/a_m/a_g/a_i
  |  withered：枯萎状态 collected：采摘状态 |   |  | 提醒浇水/施肥/除草/除虫

##/GET 以上信息
##/POST 上传土地图片
##/POST 各种提醒状态
##/POST 改变作物生长状态

#农场管理员模块
##农场>发布土地>土地列表
###plant_list
    {
        Pnum: '#qwe123',
        Pphoto: './images/head_120.png',
        Pprice: '20',   //¥
        Parea: '10' //㎡
    }

Pnum | Pphoto | Pprice | Parea 
----|------|----|----
编号 | 图片  | 价格 | 面积
char | file<512kb  | int | int

##/GET 以上信息列表
##/DELETE 删除该土地  >dalete时会有多组数据一起DELETE的情况
##/POST 增加土地  >post时会有多组数据一起上传的情况   ex:[{d1},{d2},{d3}]
##/UPDATE 更新土地信息

---

##农场>发布种子
##农场>发布种子方案 >种子列表
    {
        sName:'冬瓜',
        sPrice:'10', //¥
        sGtime:'30' //天
    }

sName | sPrice | sGtime  
----|------|----|----
种子名称 | 种子价格  | 成长时间 
char | file<512kb  | int 

##/GET 以上信息列表
##/DELETE 删除该种子 >dalete时会有多组数据一起DELETE的情况
##/POST 增加种子 >post时会有多组数据一起上传的情况   ex:[{d1},{d2},{d3}]
##/UPDATE 更新种子信息

---
##农场>发布种子方案> 种子方案
###/编辑状态时
    {
        cNum: 1,
        cContent: {
            p1: ['冬瓜', '西瓜', '南瓜', '北瓜'],
            p2: ['冬瓜', '西瓜', '南瓜', '北瓜'],
            p3: ['冬瓜', '西瓜', '南瓜', '北瓜']
        },
        isPublish: true
    }


cNum | cContent | isPublish  
----|------|----
方案编号 | 方案内容  | 是否发布 
char | 可选的作物  | bool

###/GET 以上信息列表
###/POST的数据
    {
        cNum: 1,
        cContent: {
            p1: '冬瓜',
            p2: '南瓜',
            p3: '西瓜'
        },
        isPublish: true
    }
###DELETE 删除方案

---
###sever_pck
    {
        Scon: 10,
        Sprice: 50,
        Sstatu: true
    }

Scon | Sprice | Sstatu
----|------|----
次数 | 价格  | 状态
int | int  | bool

---
###msg_tem
    {
        title: '确认信息',
        content: '确认完成当前操作',
        concel: {
            content: '取消',
            display: true
        },
        ok: {
            content: '确认',
            display: true
        }
    }

---
